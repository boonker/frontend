import * as React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { Provider as PaperProvider } from "react-native-paper";
import { LogBox } from "react-native";
import { Provider as StoreProvider } from "react-redux";
import * as Notifications from "expo-notifications";
import Geocode from "react-geocode";
import { combineReducers, configureStore } from "@reduxjs/toolkit";

import PubNub from "pubnub";
import { PubNubProvider } from "pubnub-react";

import advertisements from "./src/screens/advertisements-my/advertisements/AdvertisementsScreen.slice";
import updateAdvertisement from "./src/screens/advertisements-update/UpdateAdvertisementsScreen.slice";
import cityAdvertisements from "./src/screens/home/all-tab/AllByCityAdvertisements.slice";
import userProfileScreen from "./src/screens/user/profile/UserProfileScreen.slice";
import userRatingList from "./src/screens/user/rating/UserRating.slice";
import userFollowingList from "./src/screens/user/following/UserFollowingScreen.slice";
import userFollowersList from "./src/screens/user/followers/UserFollowersScreen.slice";
import advertisementOffers from "./src/screens/offer/AdvertisementOffersScreen.slice";
import userDrivingDeliveries from "./src/screens/deliveries/driving-packages/DrivingPackagesScreen.slice";
import oneDelivery from "./src/screens/tracking/driver-delivery-tracking/DriverTrackingScreen.slice";
import userIncomingDeliveries from "./src/screens/deliveries/incoming-packages/IncomingPackagesScreen.slice";
import LoginNavigation from "./src/navigation/LoginNavigation";
import userOffers from "./src/screens/advertisements-my/offers/MyOffersScreen.slice";
import followingAdvertisements from "./src/screens/home/all-following-tab/AllByFollowingAdvertisements.slice";
import personalInfo from "./src/screens/profile/SettingsScreen.slice";

export const allReducers = combineReducers({
  content: advertisements,
  updateAdvertisement: updateAdvertisement,
  getAllAdvertisementsByCity: cityAdvertisements,
  userDetails: userProfileScreen,
  userRatingList: userRatingList,
  userFollowingList: userFollowingList,
  userFollowersList: userFollowersList,
  advertisementOffers: advertisementOffers,
  userDrivingDeliveries: userDrivingDeliveries,
  oneDelivery: oneDelivery,
  userIncomingDeliveries: userIncomingDeliveries,
  userOffers: userOffers,
  followingAdvertisements: followingAdvertisements,
  personalInfo: personalInfo,
});

let store = configureStore({
  reducer: allReducers,
});

const uuid = PubNub.generateUUID();
const pubnub = new PubNub({
  publishKey: "pub-c-73cbe5ad-2b6b-4938-b3f3-fb82fdfc0909",
  subscribeKey: "sub-c-bd3cb0b8-f91a-11eb-8531-d287984b4121",
  uuid: uuid,
});

//this is baddddd
LogBox.ignoreLogs(["Setting a timer"]);
LogBox.ignoreAllLogs(); //Ignore all log notifications

function App() {
  Geocode.setApiKey("AIzaSyBF9LF7TC7F6dfJyVGbN_lWwpFih7lHkF8");
  const notificationListener = React.useRef();
  const responseListener = React.useRef();

  Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: true,
    }),
  });

  React.useEffect(() => {
    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        console.log("Notification", notification);
      });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        console.log("response", response);
      });

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return (
    <PubNubProvider client={pubnub}>
      <StoreProvider store={store}>
        <PaperProvider>
          <NavigationContainer>
            <LoginNavigation />
          </NavigationContainer>
        </PaperProvider>
      </StoreProvider>
    </PubNubProvider>
  );
}

export default App;
