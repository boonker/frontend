# frontend

Preduvjeti:

0. Pokrenuti Boonker backend API https://gitlab.com/boonker/backend
1. Instalirati npm manager: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
2. Na Android uređaju preuzeti i instalirati "Expo Go" aplikaciju 
3. U datoteci /frontend/src/Constants.js u varijabli `BOONKER_SERVICE_URL` zapisati privatnu IP adresu računala i spremiti promjene

Pokretanje:
1. Pozicionirati se u `/boonker/frontend direktoriji`
2. Pokrenuti `npm install --global expo-cli`
3. Pokrenuti `npm install`
3. Pokrenuti `expo start`

U defaultnom web browseru otvara se Expo sučelje s QR kodom kojeg je potrebno skenirati s Expo Go mobilnom aplikacijom
