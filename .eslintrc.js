module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["plugin:react/recommended"],
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    quotes: [2, "double", { avoidEscape: true }],
    semi: [2, "always"],
    "comma-dangle": ["error", "always-multiline"],
    "react/prop-types": "off",
  },
};
