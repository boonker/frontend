import { combineReducers } from "@reduxjs/toolkit";

import advertisements from "../screens/advertisements-my/advertisements/AdvertisementsScreen.slice";

const sharedReducers = {
  ad: advertisements,
};

export const rootReducer = combineReducers(sharedReducers);
