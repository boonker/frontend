import { configureStore, combineReducers, createStore } from "@reduxjs/toolkit";
import advertisements from "../screens/advertisements-my/advertisements/AdvertisementsScreen.slice";
import updateAdvertisement from "../screens/advertisements-update/UpdateAdvertisementsScreen.slice";

export const allReducers = combineReducers({
  updateAdvertisement: updateAdvertisement,
});

let store = configureStore({
  reducer: allReducers,
});

export default store;
