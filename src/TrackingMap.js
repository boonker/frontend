import React from "react";

import MapView, { Marker } from "react-native-maps";
import { Ionicons } from "@expo/vector-icons";

export default function TrackingMap({ location, mapHeight, mapWidth }) {
  return (
    <MapView
      style={{
        width: mapWidth,
        height: mapHeight,
        zIndex: 5,
      }}
      region={{
        latitude: location.latitude,
        longitude: location.longitude,
        latitudeDelta: 0.03,
        longitudeDelta: 0.4,
      }}
      showsTraffic={true}
      showsCompass={true}
      loadingEnabled={true}
    >
      <Marker coordinate={location} title={"Brm brm"}>
        <Ionicons name={"car"} size={30} color={"blue"} />
      </Marker>
    </MapView>
  );
}
