import { Text, TouchableOpacity } from "react-native";
import React from "react";

export default function ErrorField({ errorText }) {
  return (
    <Text
      style={{
        color: "red",
        marginHorizontal: 10,
        marginVertical: 2,
      }}
    >
      {errorText}
    </Text>
  );
}
