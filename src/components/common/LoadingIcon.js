import React from "react";

import { ActivityIndicator, View } from "react-native";

import * as Constants from "../../Constants";

export default function LoadingIcon() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  );
}
