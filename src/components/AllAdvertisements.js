import * as React from "react";

import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { Avatar, Card, Paragraph, Text } from "react-native-paper";

import * as Constants from "../Constants";

const Item = ({ item, navigation }) => {
  return (
    <TouchableOpacity
      onPress={async () => {
        navigation.navigate("OneAdvertisementScreen", {
          id: item.id,
          userId: item.userId,
        });
      }}
      activeOpacity={0.1}
      style={{
        marginVertical: 4,
        marginHorizontal: 4,
      }}
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <Card.Title
          title={item.title}
          subtitle={item.userFullName}
          left={() => (
            <Avatar.Image
              size={48}
              source={require("../screens/assets/avatar.png")}
            />
          )}
          right={() => (
            <Text
              style={{
                marginRight: 12,
                color: Constants.PRIMARY_COLOR,
                fontWeight: "bold",
              }}
            >
              {item.advertisementType}
            </Text>
          )}
        />
        <Card.Content>
          <View
            style={{
              justifyContent: "space-between",
              flexDirection: "row",
              marginBottom: 2,
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "baseline",
              }}
            >
              <Text
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
                style={styles.info}
              >
                {item.cityFrom}
              </Text>
              <Ionicons
                name="arrow-forward"
                size={16}
                color={Constants.PRIMARY_COLOR}
              />
              <Text
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
                style={styles.info}
              >
                {item.cityTo}
              </Text>
            </View>
            <Text>{item.price}kn</Text>
          </View>
          <Paragraph>{item.content}</Paragraph>
        </Card.Content>
        <Card.Cover
          source={{
            uri: "https://icdn.digitaltrends.com/image/digitaltrends/amazon-package-2-720x720.jpg",
          }}
        />
        <Card.Actions />
      </Card>
    </TouchableOpacity>
  );
};

export default function AllAdvertisements({
  navigation,
  advertisements,
  extraData,
  onRefresh,
  refresh,
}) {
  const renderItem = ({ item }) => <Item item={item} navigation={navigation} />;
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={advertisements}
        renderItem={renderItem}
        extraData={extraData}
        onRefresh={onRefresh}
        keyExtractor={(item) => item.id.toString()}
        refreshing={refresh}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
