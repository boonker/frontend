import React from "react";

import { Text, TouchableOpacity, View } from "react-native";
import { Avatar, Card } from "react-native-paper";
import call from "react-native-phone-call";

import { Ionicons } from "@expo/vector-icons";

import * as Constants from "../Constants";

export default function UserInfoCard({
  fullName,
  userId,
  phoneNumber,
  cityFrom,
  cityTo,
  navigation,
}) {
  return (
    <Card>
      <Card.Title
        title={fullName}
        left={() => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("UserProfileScreen", {
                id: userId,
              })
            }
          >
            <Avatar.Image
              size={38}
              source={require("../screens/assets/avatar.png")}
            />
          </TouchableOpacity>
        )}
        right={() => {
          return (
            <TouchableOpacity
              style={{ flexDirection: "row" }}
              onPress={() =>
                call({
                  number: phoneNumber,
                  prompt: true,
                }).catch(console.error)
              }
            >
              <Text style={{ marginRight: 4 }}>{phoneNumber}</Text>
              <Ionicons
                name="call"
                size={18}
                style={{ marginRight: 8 }}
                color={Constants.PRIMARY_COLOR}
              />
            </TouchableOpacity>
          );
        }}
      />
      <Card.Content>
        <View
          style={{
            marginTop: -12,
            marginLeft: 50,
            flexDirection: "row",
          }}
        >
          <Ionicons
            name={"location"}
            size={18}
            color={Constants.PRIMARY_COLOR}
          />
          <Text
            style={{
              fontSize: 14,
              justifyContent: "space-between",
              marginLeft: 4,
            }}
          >
            {cityFrom}
          </Text>
          <Ionicons
            name="arrow-forward"
            size={18}
            color={Constants.PRIMARY_COLOR}
          />
          <Text
            style={{
              fontSize: 14,
              justifyContent: "space-between",
            }}
          >
            {cityTo}
          </Text>
        </View>
      </Card.Content>
    </Card>
  );
}
