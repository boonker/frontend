import * as React from "react";

import { SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import { Avatar, Button } from "react-native-paper";

import { unfollowUser } from "../api/user/unfollowUser";
import { followUser } from "../api/user/followUser";
import { sendPushNotification } from "./hooks/sendPushNotification";
import { getExpoPushToken } from "../api/expo-notifications/getExpoPushToken";
import { getCurrentUser } from "../api/security/auth";

export default function UserFollowItem({
  item,
  navigation,
  currentUserId,
  onPress,
}) {
  const onUnfollow = async (id) => {
    await unfollowUser(id);
    onPress();
  };

  const onFollow = async (id) => {
    await followUser(id);
    const currentUser = await getCurrentUser();

    const token = await getExpoPushToken(id);
    await sendPushNotification(
      token.expoPushToken,
      "New follower!",
      currentUser.fullName + " just followed you!"
    );
    onPress();
  };

  return (
    <SafeAreaView
      style={{
        margin: 4,
        backgroundColor: "white",
      }}
    >
      <View
        style={{
          borderColor: "gray",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 8,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              if (item.userResponse.id !== currentUserId) {
                navigation.navigate("UserProfileScreen", {
                  id: item.userResponse.id,
                });
              } else {
                navigation.navigate("Profile");
              }
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginLeft: 8,
              }}
            >
              <Avatar.Image
                size={54}
                source={require("../screens/assets/avatar.png")}
              />
              <Text
                style={{
                  marginLeft: 4,
                }}
              >
                {item.userResponse.fullName}
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              alignItems: "flex-end",
              flex: 1,
              marginRight: 8,
            }}
          >
            {item.userResponse.id !== currentUserId ? (
              item.isFollowedByCurrentUser ? (
                <Button
                  style={{
                    width: 100,
                  }}
                  mode="contained"
                  onPress={() => onUnfollow(item.userResponse.id)}
                >
                  <Text
                    style={{
                      fontSize: 10,
                    }}
                  >
                    Unfollow
                  </Text>
                </Button>
              ) : (
                <Button
                  style={{
                    width: 100,
                  }}
                  mode="contained"
                  onPress={() => {
                    onFollow(item.userResponse.id);
                  }}
                >
                  <Text
                    style={{
                      fontSize: 10,
                    }}
                  >
                    Follow
                  </Text>
                </Button>
              )
            ) : null}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}
