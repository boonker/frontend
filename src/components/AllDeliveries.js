import * as React from "react";

import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Avatar, Button, Card } from "react-native-paper";
import call from "react-native-phone-call";

import Ionicons from "react-native-vector-icons/Ionicons";

import * as Constants from "../Constants";
import { getExpoPushToken } from "../api/expo-notifications/getExpoPushToken";
import { sendPushNotification } from "./hooks/sendPushNotification";
import { getCurrentUser } from "../api/security/auth";

const Item = ({ item, navigation, isDriving }) => {
  return (
    <SafeAreaView
      style={{
        margin: 8,
      }}
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <Card.Title
          title={isDriving ? item.forUserFullName : item.userFullName}
          left={() => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("UserProfileScreen", {
                  id: isDriving ? item.forUserId : item.userId,
                })
              }
            >
              <Avatar.Image
                size={48}
                source={require("../../src/screens/assets/avatar.png")}
              />
            </TouchableOpacity>
          )}
          right={() => {
            if (item.deliveryStatus === "FINISHED") {
              return (
                <Ionicons
                  style={{
                    marginRight: 8,
                  }}
                  name={"checkmark-circle"}
                  size={24}
                  color={"green"}
                />
              );
            }
            if (
              item.deliveryStatus === "INCOMING" ||
              item.deliveryStatus === "TODAY" ||
              item.deliveryStatus === "ACTIVE"
            ) {
              return (
                <Ionicons
                  style={{ marginRight: 8 }}
                  name="car"
                  size={24}
                  color={Constants.PRIMARY_COLOR}
                />
              );
            }
            if (item.deliveryStatus === "PAST") {
              return (
                <Ionicons
                  style={{ marginRight: 8 }}
                  name="remove-circle"
                  size={24}
                  color={Constants.PRIMARY_COLOR}
                />
              );
            }
          }}
        />
        <Card.Content>
          <View>
            <TouchableOpacity
              style={{
                marginTop: 10,
                marginLeft: 4,
                flexDirection: "row",
              }}
              onPress={() =>
                call({ number: item.forUserPhoneNumber, prompt: true }).catch(
                  console.error
                )
              }
            >
              <Ionicons name="call" size={20} color={Constants.PRIMARY_COLOR} />
              <Text
                style={{
                  fontSize: 14,
                  justifyContent: "space-between",
                  marginLeft: 4,
                }}
              >
                {isDriving ? item.forUserPhoneNumber : item.userPhoneNumber}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                marginTop: 10,
                marginLeft: 4,
                flexDirection: "row",
              }}
            >
              <Ionicons name="car" size={20} color={Constants.PRIMARY_COLOR} />
              <Text
                style={{
                  fontSize: 14,
                  justifyContent: "space-between",
                  marginLeft: 4,
                }}
              >
                {item.deliveryDate}
              </Text>
            </View>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                marginBottom: 2,
                marginTop: 4,
              }}
            >
              <View
                style={{
                  marginTop: 10,
                  flexDirection: "row",
                }}
              >
                <Ionicons
                  name={"location"}
                  size={20}
                  color={Constants.PRIMARY_COLOR}
                />
                <Text
                  style={{
                    fontSize: 14,
                    justifyContent: "space-between",
                    marginLeft: 4,
                  }}
                >
                  {item.cityFrom}, {item.cityFromAddress}
                </Text>
                <Ionicons
                  name="arrow-forward"
                  size={20}
                  color={Constants.PRIMARY_COLOR}
                />
                <Text
                  style={{
                    fontSize: 14,
                    justifyContent: "space-between",
                  }}
                >
                  {item.cityTo}, {item.cityToAddress}
                </Text>
              </View>
            </View>
            <View
              style={{
                marginTop: 10,
                marginLeft: 4,
                flexDirection: "row",
              }}
            >
              <Ionicons name="cash" size={20} color={Constants.PRIMARY_COLOR} />
              <Text
                style={{
                  fontSize: 14,
                  justifyContent: "space-between",
                  marginLeft: 4,
                }}
              >
                {item.price} kn
              </Text>
            </View>
          </View>
        </Card.Content>
        <Card.Actions style={{ marginLeft: 8 }}>
          <View style={{ flex: 1 }}>
            {isDriving &&
            (item.deliveryStatus === "INCOMING" ||
              item.deliveryStatus === "ACTIVE" ||
              item.deliveryStatus === "TODAY") ? (
              <Button
                mode={"contained"}
                disabled={item.deliveryStatus === "INCOMING"}
                onPress={async () => {
                  navigation.navigate("DriverTrackingScreen", {
                    forUserId: item.forUserId,
                    deliveryId: item.id,
                  });

                  const token = await getExpoPushToken(item.forUserId);
                  const currentUser = await getCurrentUser();

                  await sendPushNotification(
                    token.expoPushToken,
                    "Package is arriving!",
                    currentUser.fullName +
                      " started your delivery, check it out!"
                  );
                }}
              >
                {item.deliveryStatus === "ACTIVE"
                  ? "View driving"
                  : "Start driving"}
              </Button>
            ) : null}
            {!isDriving &&
            (item.deliveryStatus === "ACTIVE" ||
              item.deliveryStatus === "TODAY" ||
              item.deliveryStatus === "INCOMING") ? (
              <Button
                mode={"contained"}
                disabled={item.deliveryStatus !== "ACTIVE"}
                onPress={() =>
                  navigation.navigate("IncomingDeliveryTracking", {
                    userId: item.userId,
                    deliveryId: item.id,
                  })
                }
              >
                Track your package!
              </Button>
            ) : null}
          </View>
        </Card.Actions>
      </Card>
    </SafeAreaView>
  );
};

export default function AllDeliveries({
  navigation,
  deliveries,
  isDriving,
  refresh,
  onRefresh,
}) {
  const renderItem = ({ item }) => (
    <Item item={item} navigation={navigation} isDriving={isDriving} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={deliveries}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        refreshing={refresh}
        onRefresh={onRefresh}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    marginLeft: 8,
  },
});
