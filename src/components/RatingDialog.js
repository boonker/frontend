import React from "react";

import { Alert, SafeAreaView } from "react-native";
import { Button, Dialog, Portal, TextInput } from "react-native-paper";
import { Rating } from "react-native-ratings";

import { Formik } from "formik";

import * as Constants from "../Constants";

import { newUserRating } from "../api/rating/newUserRating";
import { setFinishedDelivery } from "../api/delivery/setFinishedDelivery";
import { getExpoPushToken } from "../api/expo-notifications/getExpoPushToken";
import { sendPushNotification } from "./hooks/sendPushNotification";
import { getCurrentUser } from "../api/security/auth";

export default function RatingDialog({
  visible,
  onDismiss,
  userId,
  deliveryId,
  isDriving,
}) {
  return (
    <Portal>
      <Formik
        initialValues={{
          ratingScore: "0",
          review: "",
        }}
        onSubmit={async (values, { resetForm }) => {
          await newUserRating({
            forUserId: userId,
            review: values.review,
            score: values.ratingScore,
          });

          Alert.alert("Successfully rated!", "Thank you", [{ text: "OK" }]);

          await setFinishedDelivery(deliveryId);
          const currentUser = await getCurrentUser();

          const token = await getExpoPushToken(userId);
          await sendPushNotification(
            token.expoPushToken,
            "Rating",
            currentUser.fullName + " rated you!"
          );
          onDismiss();
          resetForm();
        }}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
        }) => (
          <Dialog visible={visible} onDismiss={onDismiss}>
            <Dialog.Title>Rate user</Dialog.Title>
            <Dialog.Content>
              <SafeAreaView
                style={{
                  alignItems: "center",
                }}
              >
                <Rating
                  showRating={true}
                  onFinishRating={(rating) =>
                    setFieldValue("ratingScore", rating)
                  }
                  style={{ paddingVertical: 10 }}
                  fractions={2}
                  jumpValue={0.5}
                />
                <TextInput
                  value={values.review}
                  placeholder={"Review"}
                  onChangeText={handleChange("review")}
                  onBlur={handleBlur("review")}
                  numberOfLines={8}
                  style={{
                    width: 240,
                    height: 120,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                mode={"contained"}
                color={Constants.PRIMARY_COLOR}
                onPress={handleSubmit}
              >
                Submit
              </Button>
              <Button
                mode={"contained"}
                color={Constants.SECONDARY_COLOR}
                onPress={onDismiss}
                style={{ marginHorizontal: 4 }}
              >
                Cancel
              </Button>
            </Dialog.Actions>
          </Dialog>
        )}
      </Formik>
    </Portal>
  );
}
