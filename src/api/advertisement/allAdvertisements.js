import api from "../common/api";

import authHeader from "../security/auth-header";

export async function allAdvertisements(searchAdvertisementQueryRequest) {
  console.log("request", searchAdvertisementQueryRequest);
  const authorization = await authHeader();

  const response = await api.post(
    "/advertisements/all",
    searchAdvertisementQueryRequest,
    {
      headers: authorization,
    }
  );

  console.log("FOLLOWING", response);

  return response.data;
}
