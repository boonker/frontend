import api from "../common/api";

import authHeader from "../security/auth-header";

export async function newAdvertisement(newAdvertisementRequest) {
  console.log("newAd", newAdvertisementRequest);
  const authorization = await authHeader();

  return await api.post("/advertisements/new", newAdvertisementRequest, {
    headers: authorization,
  });
}
