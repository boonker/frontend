import authHeader from "../security/auth-header";
import api from "../common/api";

export async function updateAdvertisement(id, updateAdvertisementRequest) {
  console.log("FUNKCIJA", updateAdvertisementRequest);
  const authorization = await authHeader();
  const url = "/advertisements/" + id + "/update";

  return await api.put(url, updateAdvertisementRequest, {
    headers: authorization,
  });
}
