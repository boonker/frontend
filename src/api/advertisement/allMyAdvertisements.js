import api from "../common/api";

import authHeader from "../security/auth-header";

export async function allMyAdvertisements() {
  const authorization = await authHeader();

  const response = await api.get("/advertisements/my", {
    headers: authorization,
  });

  return response.data;
}
