import api from "../common/api";

import authHeader from "../security/auth-header";

export async function deleteAdvertisement(id) {
  const authorization = await authHeader();

  return await api.delete("/advertisements/" + id + "/delete", {
    headers: authorization,
  });
}
