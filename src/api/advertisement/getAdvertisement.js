import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getAdvertisement(id) {
  const authorization = await authHeader();

  const response = await api.get("/advertisements/" + id, {
    headers: authorization,
  });

  return response.data;
}
