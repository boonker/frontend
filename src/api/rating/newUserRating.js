import api from "../common/api";

import authHeader from "../security/auth-header";

export async function newUserRating(newRatingRequest) {
  const authorization = await authHeader();

  return await api.post(
    "/ratings/new",
    { ...newRatingRequest },
    {
      headers: authorization,
    }
  );
}
