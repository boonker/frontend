import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getUserRating(id) {
  const authorization = await authHeader();

  const response = await api.get("/ratings/user/" + id, {
    headers: authorization,
  });

  return response.data;
}
