import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getUserFollowing(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id + "/following", {
    headers: authorization,
  });

  return response.data;
}
