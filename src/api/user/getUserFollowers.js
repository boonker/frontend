import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getUserFollowers(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id + "/followers", {
    headers: authorization,
  });

  return response.data;
}
