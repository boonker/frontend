import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getUser(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id, {
    headers: authorization,
  });

  return response.data;
}
