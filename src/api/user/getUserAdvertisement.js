import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getUserAdvertisement(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id + "/advertisement", {
    headers: authorization,
  });
  return response.data;
}
