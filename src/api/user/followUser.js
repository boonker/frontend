import authHeader from "../security/auth-header";

import api from "../common/api";

export async function followUser(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id + "/follow", {
    headers: authorization,
  });

  return response.data;
}
