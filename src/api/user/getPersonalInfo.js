import authHeader from "../security/auth-header";

import api from "../common/api";

export async function getPersonalInfo() {
  const authorization = await authHeader();

  const response = await api.get("/users/info", {
    headers: authorization,
  });

  return response.data;
}
