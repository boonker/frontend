import authHeader from "../security/auth-header";

import api from "../common/api";

export async function unfollowUser(id) {
  const authorization = await authHeader();

  const response = await api.get("/users/" + id + "/unfollow", {
    headers: authorization,
  });

  return response.data;
}
