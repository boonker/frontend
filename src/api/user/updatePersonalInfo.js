import authHeader from "../security/auth-header";

import api from "../common/api";

export async function updatePersonalInfo(updatePersonalInfo) {
  const authorization = await authHeader();

  const response = await api.put("/users/info", updatePersonalInfo, {
    headers: authorization,
  });

  return response.data;
}
