import axios from "axios";

import StorageService from "../common/storage";
import { BOONKER_SERVICE_AUTH_URL } from "../../Constants";

const register = async (registrationRequest) => {
  try {
    const response = await axios.post(
      BOONKER_SERVICE_AUTH_URL + "/register",
      registrationRequest
    );
    let messages = "";
    response.data.forEach((error) => {
      messages += error.message + "\n";
    });
    return { message: messages };
  } catch (e) {
    console.log("Error: register", e.getMessage());
  }
};

const login = async (username, password) => {
  try {
    console.log(BOONKER_SERVICE_AUTH_URL + "/login");
    const response = await axios.post(BOONKER_SERVICE_AUTH_URL + "/login", {
      username,
      password,
    });
    if (response.data.token) {
      await StorageService.storeData(
        "user",
        JSON.stringify(response.data)
      ).catch((r) => console.log(r));
    }
    return { status: response.status, data: response.data, error: false };
  } catch (e) {
    return { error: true };
  }
};

const logout = async () => {
  try {
    await StorageService.removeData("user");
  } catch (e) {
    console.log("Error: removing data", e.getMessage());
  }
};

export const getCurrentUser = async () => {
  try {
    const data = await StorageService.getData("user");
    return JSON.parse(data);
  } catch (e) {}
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};
