import StorageService from "../common/storage";

export default async function authHeader() {
  const data = await StorageService.getData("user");
  const user = JSON.parse(data);

  if (user && user.token) {
    return { Authorization: "Bearer " + user.token };
  } else {
    return {};
  }
}
