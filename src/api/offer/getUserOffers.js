import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getUserOffers() {
  const authorization = await authHeader();

  const response = await api.get("/offers/my", {
    headers: authorization,
  });

  return response.data;
}
