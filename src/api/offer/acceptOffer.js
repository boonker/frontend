import api from "../common/api";

import authHeader from "../security/auth-header";

export async function acceptOffer(offerId) {
  const authorization = await authHeader();

  await api.get("/offers/" + offerId + "/accept", {
    headers: authorization,
  });
}
