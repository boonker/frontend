import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getAllAdvertisementOffers(advertisementId) {
  const authorization = await authHeader();

  const response = await api.get("/offers/" + advertisementId, {
    headers: authorization,
  });

  return response.data;
}
