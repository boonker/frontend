import api from "../common/api";

import authHeader from "../security/auth-header";

export async function declineOffer(offerId) {
  const authorization = await authHeader();

  await api.get("/offers/" + offerId + "/decline", {
    headers: authorization,
  });
}
