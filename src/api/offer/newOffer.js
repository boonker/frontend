import api from "../common/api";

import authHeader from "../security/auth-header";

export async function newOffer(advertisementId, newOfferRequest) {
  const authorization = await authHeader();
  console.log(advertisementId, newOfferRequest);
  return await api.post(
    "/offers/new",
    { advertisementId, ...newOfferRequest },
    {
      headers: authorization,
    }
  );
}
