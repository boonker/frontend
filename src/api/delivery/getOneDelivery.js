import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getOneDelivery(deliveryId) {
  const authorization = await authHeader();

  const response = await api.get("/deliveries/" + deliveryId, {
    headers: authorization,
  });

  return response.data;
}
