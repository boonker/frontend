import api from "../common/api";

import authHeader from "../security/auth-header";

export async function setFinishedDelivery(deliveryId) {
  const authorization = await authHeader();

  const response = await api.get("/deliveries/finish/" + deliveryId, {
    headers: authorization,
  });

  return response.data;
}
