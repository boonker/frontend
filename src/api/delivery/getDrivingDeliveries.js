import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getDrivingDeliveries() {
  const authorization = await authHeader();

  const response = await api.get("/deliveries/driving", {
    headers: authorization,
  });

  return response.data;
}
