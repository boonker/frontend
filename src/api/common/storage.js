import AsyncStorage from "@react-native-async-storage/async-storage";

const useUser = () => {
  const [user, setUser] = Reaact.useState(userObservable.get());

  React.useEffect(async () => {
    const data = await StorageService.getData("user");
    const userData = JSON.parse(data);
    if (response !== null) {
      console.log("EFEKT", userData);
      setUser(userData);
    }
  }, [user]);

  return user;
};

const storeData = async (key, value) => {
  await removeData(key);
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log("Error while saving", e.getMessage());
  }
};

export const getData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value === null) {
    }
    return value;
  } catch (e) {
    console.log("Error reading data", e.getMessage());
  }
};

const removeData = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    console.log("Error deleting data", e.getMessage());
  }
};

export default {
  storeData,
  getData,
  removeData,
  useUser,
};
