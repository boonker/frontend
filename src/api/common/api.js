import axios from "axios";
import { BOONKER_SERVICE_API_URL } from "../../Constants";

export default axios.create({
  baseURL: BOONKER_SERVICE_API_URL,
});
