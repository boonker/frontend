import api from "../common/api";

import authHeader from "../security/auth-header";

export async function setExpoPushToken(expoPushToken) {
  const authorization = await authHeader();

  const response = await api.post(
    "/users/expo",
    {
      expoPushToken: expoPushToken,
    },
    {
      headers: authorization,
    }
  );

  return response.data;
}
