import api from "../common/api";

import authHeader from "../security/auth-header";

export async function getExpoPushToken(id) {
  const authorization = await authHeader();
  const response = await api.get("/users/expo/" + id, {
    headers: authorization,
  });

  return response.data;
}
