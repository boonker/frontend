export const PRIMARY_COLOR = "#6200ee";
export const SECONDARY_COLOR = "#DDA0DD";

export const BOONKER_SERVICE_URL = "http://192.168.0.17:8080/";
export const BOONKER_SERVICE_API_URL = BOONKER_SERVICE_URL + "boonker/api";
export const BOONKER_SERVICE_AUTH_URL = BOONKER_SERVICE_URL + "api/auth";
