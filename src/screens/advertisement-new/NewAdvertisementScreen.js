import * as React from "react";

import * as Constants from "../../Constants";

import DateTimePicker from "@react-native-community/datetimepicker";
import { Formik } from "formik";
import { Ionicons } from "@expo/vector-icons";

import {
  Alert,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import { newAdvertisement } from "../../api/advertisement/newAdvertisement";
import { getAllMyAdvertisements } from "../advertisements-my/advertisements/AdvertisementsScreen.slice";
import SelectDropdown from "react-native-select-dropdown";
import { Button, TextInput } from "react-native-paper";

import { useDispatch } from "react-redux";
import ErrorField from "../../components/common/ErrorField";

import * as Yup from "yup";

const NewAdvertisementSchema = Yup.object().shape({
  title: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  content: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(200, "Too song! Should be 200 chars max")
    .required("Required!"),
  price: Yup.number().required("Required!"),
  deliveryDate: Yup.string().required("Required!"),
  cityFrom: Yup.string().required("Required!"),
  cityTo: Yup.string().required("Required!"),
  packageSize: Yup.number().required("Required!"),
});

export default function NewAdvertisementScreen() {
  const advertisementTypes = ["DRIVING", "SENDING"];
  const dispatch = useDispatch();
  const [deliveryDate, setDeliveryDate] = React.useState(new Date());
  const [show, setShow] = React.useState(false);
  const [selectedType, setSelectedType] = React.useState("");

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || deliveryDate;
    setShow(Platform.OS === "ios");
    setDeliveryDate(currentDate);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: "center",
        paddingVertical: 10,
      }}
    >
      <SafeAreaView>
        <Formik
          initialValues={{
            title: "",
            content: "",
            price: "",
            deliveryDate: "",
            cityFrom: "",
            cityFromAddress: "",
            cityTo: "",
            cityToAddress: "",
            packageSize: "",
          }}
          onSubmit={async (values, { resetForm }) => {
            await newAdvertisement({
              ...values,
              advertisementType: selectedType,
            });
            dispatch(getAllMyAdvertisements());

            Alert.alert(
              "New advertisement",
              "Successfully created a new ad, good luck!"
            );
            resetForm();
          }}
          validationSchema={NewAdvertisementSchema}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
            setFieldValue,
          }) => (
            <SafeAreaView
              style={{
                alignItems: "center",
              }}
            >
              <SafeAreaView style={style.textInputView}>
                {errors.title && touched.title ? (
                  <ErrorField errorText={errors.title} />
                ) : null}
                <TextInput
                  value={values.title}
                  placeholder={"Title"}
                  onChangeText={handleChange("title")}
                  onBlur={handleBlur("title")}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.content && touched.content ? (
                  <ErrorField errorText={errors.content} />
                ) : null}
                <TextInput
                  value={values.content}
                  placeholder={"Content"}
                  multiline={true}
                  numberOfLines={4}
                  onChangeText={handleChange("content")}
                  onBlur={handleBlur("content")}
                  style={{
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.price && touched.price ? (
                  <ErrorField errorText={errors.price} />
                ) : null}
                <TextInput
                  value={values.price}
                  placeholder={"Price (kn)"}
                  keyboardType="numeric"
                  onChangeText={handleChange("price")}
                  onBlur={handleBlur("price")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView
                style={{
                  alignItems: "flex-start",
                  width: 300,
                  marginBottom: 12,
                }}
              >
                <SelectDropdown
                  buttonStyle={{
                    width: 200,
                    height: 40,
                    borderRadius: 20,
                    alignItems: "center",
                    backgroundColor: "#DCDCDC",
                  }}
                  buttonTextStyle={{
                    fontSize: 16,
                    color: "gray",
                  }}
                  defaultButtonText={"Advertisement type"}
                  data={advertisementTypes}
                  onSelect={(selectedItem) => {
                    setSelectedType(selectedItem);
                  }}
                  dropdownStyle={{
                    borderRadius: 20,
                  }}
                  buttonTextAfterSelection={(selectedItem) => {
                    return selectedItem;
                  }}
                  rowTextForSelection={(item) => {
                    return item;
                  }}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.cityFrom && touched.cityFrom ? (
                  <ErrorField errorText={errors.cityFrom} />
                ) : null}
                <TextInput
                  value={values.cityFrom}
                  placeholder={"City from"}
                  onChangeText={handleChange("cityFrom")}
                  onBlur={handleBlur("cityFrom")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                  name={"from"}
                />
              </SafeAreaView>
              {selectedType === "DRIVING" ? (
                <SafeAreaView style={style.textInputView}>
                  {errors.cityFromAddress && touched.cityFromAddress ? (
                    <ErrorField errorText={errors.cityFromAddress} />
                  ) : null}
                  <TextInput
                    value={values.cityFromAddress}
                    placeholder={"Pickup address"}
                    onChangeText={handleChange("cityFromAddress")}
                    onBlur={handleBlur("cityFromAddress")}
                    style={style.textInputStyle}
                    underlineColor={"transparent"}
                    name={"cityFromAddress"}
                  />
                </SafeAreaView>
              ) : (
                <>
                  <SafeAreaView style={style.textInputView}>
                    {errors.cityFromAddress && touched.cityFromAddress ? (
                      <ErrorField errorText={errors.cityFromAddress} />
                    ) : null}
                    <TextInput
                      value={values.cityFromAddress}
                      placeholder={"City from address"}
                      onChangeText={handleChange("cityFromAddress")}
                      onBlur={handleBlur("cityFromAddress")}
                      style={style.textInputStyle}
                      underlineColor={"transparent"}
                      name={"cityFromAddress"}
                    />
                  </SafeAreaView>
                </>
              )}
              <SafeAreaView style={style.textInputView}>
                {errors.cityTo && touched.cityTo ? (
                  <ErrorField errorText={errors.cityTo} />
                ) : null}
                <TextInput
                  value={values.cityTo}
                  placeholder={"City to"}
                  onChangeText={handleChange("cityTo")}
                  onBlur={handleBlur("cityTo")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                  name={"to"}
                />
              </SafeAreaView>
              {selectedType === "SENDING" ? (
                <SafeAreaView style={style.textInputView}>
                  {errors.cityToAddress && touched.cityToAddress ? (
                    <ErrorField errorText={errors.cityToAddress} />
                  ) : null}
                  <TextInput
                    value={values.cityFromAddress}
                    placeholder={"City to address"}
                    onChangeText={handleChange("cityToAddress")}
                    onBlur={handleBlur("cityToAddress")}
                    style={style.textInputStyle}
                    underlineColor={"transparent"}
                    name={"cityToAddress"}
                  />
                </SafeAreaView>
              ) : null}
              <SafeAreaView style={style.textInputView}>
                {errors.packageSize && touched.packageSize ? (
                  <ErrorField errorText={errors.packageSize} />
                ) : null}
                <TextInput
                  value={values.packageSize}
                  placeholder={"Package size"}
                  onChangeText={handleChange("packageSize")}
                  onBlur={handleBlur("packageSize")}
                  style={style.textInputStyle}
                  keyboardType={"numeric"}
                  underlineColor={"transparent"}
                  name={"packageSize"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.packageSize && touched.packageSize ? (
                  <ErrorField errorText={errors.packageSize} />
                ) : null}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: 300,
                    marginBottom: 30,
                  }}
                >
                  <TextInput
                    value={values.deliveryDate}
                    placeholder={"Expected delivery date"}
                    editable={false}
                    onChangeText={handleChange("date")}
                    onBlur={handleBlur("date")}
                    style={style.dateInputStyle}
                    underlineColor={"transparent"}
                  />

                  <TouchableOpacity
                    style={{
                      height: 60,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onPress={showDatepicker}
                  >
                    <Ionicons
                      name={"calendar"}
                      size={24}
                      color={Constants.PRIMARY_COLOR}
                      onPress={showDatepicker}
                      style={{
                        marginLeft: 4,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                {show && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={deliveryDate}
                    mode={"date"}
                    display="default"
                    onChange={(event, selectedDate) => {
                      onChange(event, selectedDate);
                      setFieldValue(
                        "deliveryDate",
                        selectedDate
                          .toISOString()
                          .replace(/T.*/, "")
                          .split("-")
                          .join("-")
                      );
                    }}
                  />
                )}
              </SafeAreaView>
              <Button
                onPress={handleSubmit}
                mode="contained"
                contentStyle={{
                  width: 200,
                }}
                style={{
                  marginTop: -28,
                }}
              >
                Submit
              </Button>
            </SafeAreaView>
          )}
        </Formik>
      </SafeAreaView>
    </ScrollView>
  );
}

const style = StyleSheet.create({
  textInputStyle: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  dateInputStyle: {
    height: 40,
    width: 300,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textInputView: { alignItems: "flex-start" },
});
