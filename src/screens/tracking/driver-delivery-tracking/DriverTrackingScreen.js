import React from "react";

import { Alert, Dimensions, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";

import * as Location from "expo-location";
import { usePubNub } from "pubnub-react";

import { getDelivery } from "./DriverTrackingScreen.slice";
import { setActiveDelivery } from "../../../api/delivery/setActiveDelivery";
import UserInfoCard from "../../../components/UserInfoCard";
import RatingDialog from "../../../components/RatingDialog";
import TrackingMap from "../../../TrackingMap";
import LoadingIcon from "../../../components/common/LoadingIcon";
import { setFinishedDelivery } from "../../../api/delivery/setFinishedDelivery";
import { getCurrentUser } from "../../../api/security/auth";
import { sendPushNotification } from "../../../components/hooks/sendPushNotification";
import { getExpoPushToken } from "../../../api/expo-notifications/getExpoPushToken";

export default function DriverTrackingScreen({ route, navigation }) {
  const { forUserId, deliveryId } = route.params;
  const pubnub = usePubNub();
  const dispatch = useDispatch();

  const [location, setLocation] = React.useState({ latitude: 0, longitude: 0 });
  const [mapLoading, setMapFinishedLoading] = React.useState(false);
  const [loadingData, setFinishedLoadingData] = React.useState(false);
  const [visible, setVisible] = React.useState(false);
  const [isFinishedDriving, setIsFinishedDriving] = React.useState(false);

  const channelName = "driving." + deliveryId;
  const MAP_WIDTH = Dimensions.get("window").width;
  const MAP_HEIGHT = Dimensions.get("window").height / 1.6;
  const BUTTON_HEIGHT = Dimensions.get("window").height - MAP_HEIGHT;

  React.useEffect(() => {
    setFinishedLoadingData(false);

    async function fetchDelivery() {
      await setActiveDelivery(deliveryId);
      await dispatch(getDelivery(deliveryId));
      setFinishedLoadingData(true);
    }

    fetchDelivery();
  }, [dispatch]);

  const { delivery } = useSelector((state) => state.oneDelivery);

  const showDialog = async () => {
    setIsFinishedDriving(true);

    await setFinishedDelivery(deliveryId);
    const currentUser = await getCurrentUser();
    const token = await getExpoPushToken(forUserId);
    await sendPushNotification(
      token.expoPushToken,
      "Delivery arrived",
      currentUser.fullName + "delivered your package!"
    );

    pubnub.publish(
      {
        channel: channelName,
        message: {
          isFinished: true,
        },
      },
      function (status, response) {
        console.log("PUBLISH", status, response);
      }
    );
    setVisible(true);
  };
  const hideDialog = () => setVisible(false);

  React.useEffect(() => {
    setMapFinishedLoading(false);

    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        Alert.alert(
          "Location access",
          "Permission to access location was denied",
          [{ text: "OK" }]
        );
        return;
      }

      await Location.watchPositionAsync({}, (location) => {
        setLocation({
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
        });
        if (isFinishedDriving === false) {
          pubnub.publish(
            {
              channel: channelName,
              message: {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
              },
            },
            function (status, response) {
              //   console.log("PUBLISH", status, response);
              //   console.log("channelName", channelName);
            }
          );
        }
      });

      setMapFinishedLoading(true);
    })();
  }, []);

  return mapLoading === false && loadingData === false ? (
    <LoadingIcon />
  ) : (
    <View>
      <UserInfoCard
        navigation={navigation}
        userId={delivery.forUserId}
        fullName={delivery.forUserFullName}
        phoneNumber={delivery.forUserPhoneNumber}
        cityTo={
          loadingData
            ? delivery.cityTo.concat(", ", delivery.cityToAddress)
            : ""
        }
        cityFrom={
          loadingData
            ? delivery.cityFrom.concat(", ", delivery.cityFromAddress)
            : ""
        }
      />
      {isFinishedDriving === false ? (
        <TrackingMap
          location={location}
          mapWidth={MAP_WIDTH}
          mapHeight={MAP_HEIGHT}
        />
      ) : (
        <View
          style={{
            height: MAP_HEIGHT,
            width: MAP_WIDTH,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text>Finished driving</Text>
        </View>
      )}
      <View style={{ height: BUTTON_HEIGHT }}>
        <Button
          mode={"contained"}
          style={{ height: BUTTON_HEIGHT }}
          labelStyle={{ alignContent: "center" }}
          onPress={() => {
            Alert.alert("Finish driving", "Are you sure?", [
              { text: "Yes", onPress: () => showDialog() },
              { text: "Cancel" },
            ]);
          }}
          disabled={isFinishedDriving}
          color={"red"}
        >
          Finish driving
        </Button>
      </View>
      <RatingDialog
        onDismiss={() => hideDialog()}
        visible={visible}
        userId={forUserId}
        deliveryId={deliveryId}
      />
    </View>
  );
}
