import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getOneDelivery } from "../../../api/delivery/getOneDelivery";

export const getDelivery = createAsyncThunk(
  "getDelivery",
  async (deliveryId) => await getOneDelivery(deliveryId)
);

export const initialState = {
  delivery: {},
  loading: false,
  error: null,
};

export const oneDeliverySlice = createSlice({
  name: "oneDeliverySlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getDelivery.fulfilled, (state, action) => {
      state.delivery = action.payload;
    });
  },
});

const oneDelivery = oneDeliverySlice.reducer;

export default oneDelivery;
