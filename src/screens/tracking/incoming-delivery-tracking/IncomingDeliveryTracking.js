import React from "react";

import { Alert, Dimensions, Text, View } from "react-native";
import { usePubNub } from "pubnub-react";
import { useDispatch, useSelector } from "react-redux";

import { getDelivery } from "../driver-delivery-tracking/DriverTrackingScreen.slice";
import UserInfoCard from "../../../components/UserInfoCard";
import RatingDialog from "../../../components/RatingDialog";
import TrackingMap from "../../../TrackingMap";
import LoadingIcon from "../../../components/common/LoadingIcon";

export default function IncomingDeliveryTracking({ route, navigation }) {
  const { deliveryId } = route.params;

  const dispatch = useDispatch();
  const pubnub = usePubNub();
  const channelName = "driving." + deliveryId;
  const [channels] = React.useState([channelName]);
  const [location, setLocation] = React.useState({ latitude: 0, longitude: 0 });
  const [isFinishedDriving, setIsFinishedDriving] = React.useState(false);
  const [visible, setVisible] = React.useState(false);
  const [mapLoading, setFinishedMapLoading] = React.useState(false);
  const [dataLoading, setFinishedDataLoading] = React.useState(false);

  const MAP_WIDTH = Dimensions.get("window").width;
  const MAP_HEIGHT = Dimensions.get("window").height / 1.2;

  const showDialog = () => {
    setVisible(true);
  };
  const hideDialog = () => setVisible(false);

  React.useEffect(() => {
    setFinishedDataLoading(false);
    async function fetchDelivery() {
      await dispatch(getDelivery(deliveryId));
    }

    fetchDelivery();
    setFinishedDataLoading(true);
  }, [dispatch]);

  const { delivery } = useSelector((state) => state.oneDelivery);

  const unsubscribePubNubChannels = (listener) => {
    pubnub.removeListener(listener);
    pubnub.unsubscribe({
      channels: channels,
    });
  };

  const subscribeChannels = () => {
    pubnub.subscribe({
      channels: channels,
    });

    const listener = {
      message(msg) {
        const { message } = msg;
        console.log("PORUKA", message);
        if (message.hasOwnProperty("isFinished")) {
          setIsFinishedDriving(true);
          Alert.alert("Package status", "Package has arrived!", [
            { text: "OK", onPress: () => showDialog() },
            { text: "Cancel" },
          ]);
        } else {
          setLocation({
            latitude: message.latitude,
            longitude: message.longitude,
          });
          console.log("ELSE");
        }
      },
    };
    pubnub.addListener(listener);
    return listener;
  };

  React.useEffect(() => {
    if (isFinishedDriving === false) {
      setFinishedMapLoading(false);
      const listener = subscribeChannels();
      setFinishedMapLoading(true);
      return () => {
        unsubscribePubNubChannels(listener);
      };
    }
  }, [pubnub, channels]);

  return mapLoading === false && dataLoading === false ? (
    <LoadingIcon />
  ) : (
    <View>
      <UserInfoCard
        navigation={navigation}
        userId={delivery.userId}
        fullName={delivery.userFullName}
        phoneNumber={delivery.userPhoneNumber}
        cityTo={
          dataLoading ? delivery.cityTo + " ," + delivery.cityToAddress : ""
        }
        cityFrom={
          dataLoading ? delivery.cityFrom + " ," + delivery.cityFromAddress : ""
        }
      />
      {isFinishedDriving === false ? (
        <TrackingMap
          location={location}
          mapWidth={MAP_WIDTH}
          mapHeight={MAP_HEIGHT}
        />
      ) : (
        <View
          style={{
            height: MAP_HEIGHT,
            width: MAP_WIDTH,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text>Finished driving</Text>
        </View>
      )}
      <RatingDialog
        deliveryId={deliveryId}
        userId={delivery.userId}
        visible={visible}
        onDismiss={() => hideDialog()}
        isDriving={false}
      />
    </View>
  );
}
