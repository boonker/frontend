import * as React from "react";

import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getUserRatingList } from "./UserRating.slice";
import { Avatar, Card, IconButton } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";

const ScoreIcons = ({ scoreNumber }) => {
  switch (scoreNumber) {
    case 2:
      return (
        <>
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
        </>
      );
    case 3:
      return (
        <>
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
        </>
      );
    case 4:
      return (
        <>
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
        </>
      );
    case 5:
      return (
        <>
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
          <Ionicons name={"star"} size={22} color={"yellow"} />
        </>
      );
    default:
      return <Ionicons name={"star"} size={22} color={"yellow"} />;
  }
};
const Item = ({ item, navigation }) => {
  return (
    <SafeAreaView
      style={{
        margin: 4,
        borderRadius: 20,
      }}
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <TouchableOpacity
          onPress={() => {
            console.log("ID", item.createdBy.id);
            navigation.navigate("UserProfileScreen", { id: item.createdBy.id });
          }}
        >
          <Card.Title
            title={item.createdBy.fullName}
            subtitle={item.publishedOn}
            left={() => (
              <Avatar.Image
                size={48}
                source={require("../../assets/avatar.png")}
              />
            )}
          />
        </TouchableOpacity>
        <Card.Content>
          <SafeAreaView
            style={{
              flexDirection: "row",
              marginLeft: 8,
            }}
          >
            <ScoreIcons scoreNumber={item.score} />
          </SafeAreaView>
          <Text
            style={{
              marginLeft: 8,
              marginTop: 4,
              fontStyle: "italic",
            }}
          >
            {item.review}
          </Text>
        </Card.Content>
        <Card.Actions />
      </Card>
    </SafeAreaView>
  );
};

export default function UserRatingScreen({ route, navigation }) {
  const { id } = route.params;

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getUserRatingList(id));
  }, [dispatch]);

  const { ratings } = useSelector((state) => state.userRatingList);

  const renderItem = ({ item }) => <Item item={item} navigation={navigation} />;
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={ratings}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
