import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getUserRating } from "../../../api/rating/getUserRating";

export const getUserRatingList = createAsyncThunk(
  "getUserRatingList",
  async (id) => await getUserRating(id)
);

export const initialState = {
  ratings: [],
  error: null,
};

export const getUserRatingListSlice = createSlice({
  name: "getUserRatingListSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserRatingList.fulfilled, (state, action) => {
      state.ratings = action.payload;
    });
  },
});

const userRatingList = getUserRatingListSlice.reducer;

export default userRatingList;
