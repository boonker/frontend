import * as React from "react";

import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  View,
} from "react-native";

import * as Constants from "../../../Constants";

import { useDispatch, useSelector } from "react-redux";

import { getUserFollowersList } from "./UserFollowersScreen.slice";
import UserFollowItem from "../../../components/UserFollowItem";
import { getCurrentUser } from "../../../api/security/auth";

export default function UserFollowersScreen({ route, navigation }) {
  const { id } = route.params;
  const dispatch = useDispatch();
  const [loading, setFinishedLoading] = React.useState(false);
  const [currentUser, setCurrentUser] = React.useState("");
  const [selectedId, setSelectedId] = React.useState(null);

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchUserData() {
      await dispatch(getUserFollowersList(id));
      var userData = await getCurrentUser();
      setCurrentUser(userData.id);

      setFinishedLoading(true);
    }

    fetchUserData();
  }, [dispatch, id, selectedId]);

  const { followers } = useSelector((state) => state.userFollowersList);

  const renderItem = ({ item }) => (
    <UserFollowItem
      item={item}
      navigation={navigation}
      userId={id}
      onPress={() => setSelectedId(Math.random())}
      currentUserId={currentUser}
    />
  );

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <SafeAreaView style={styles.container}>
      <FlatList
        extraData={selectedId}
        renderItem={renderItem}
        keyExtractor={(item) => item.userResponse.id.toString()}
        data={followers}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
