import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getUserFollowers } from "../../../api/user/getUserFollowers";

export const getUserFollowersList = createAsyncThunk(
  "getUserFollowersList",
  async (id) => await getUserFollowers(id)
);

export const initialState = {
  followers: [],
  error: null,
};

export const userFollowersListSlice = createSlice({
  name: "userFollowersListSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserFollowersList.fulfilled, (state, action) => {
      console.log("ACTION", action);
      state.followers = action.payload;
    });
  },
});

const userFollowersList = userFollowersListSlice.reducer;

export default userFollowersList;
