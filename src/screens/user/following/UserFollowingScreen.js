import * as React from "react";

import { FlatList, SafeAreaView, StyleSheet } from "react-native";

import { useDispatch, useSelector } from "react-redux";

import { getUserFollowingList } from "./UserFollowingScreen.slice";
import { getCurrentUser } from "../../../api/security/auth";
import UserFollowItem from "../../../components/UserFollowItem";
import LoadingIcon from "../../../components/common/LoadingIcon";
import { Searchbar } from "react-native-paper";

export default function UserFollowingScreen({ route, navigation }) {
  const { id } = route.params;
  const dispatch = useDispatch();
  const [loading, setFinishedLoading] = React.useState(false);
  const [currentUser, setCurrentUser] = React.useState("");
  const [selectedId, setSelectedId] = React.useState(null);

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchUserData() {
      await dispatch(getUserFollowingList(id));
      var userData = await getCurrentUser();
      setCurrentUser(userData.id);

      setFinishedLoading(true);
    }

    fetchUserData();
  }, [dispatch, id, selectedId]);

  const { following } = useSelector((state) => state.userFollowingList);

  const renderItem = ({ item }) => (
    <UserFollowItem
      item={item}
      navigation={navigation}
      userId={id}
      onPress={() => setSelectedId(Math.random())}
      currentUserId={currentUser}
    />
  );

  const renderHeader = () => {
    return (
      <Searchbar
        placeholder="Type Here..."
        lightTheme
        round
        onChangeText={(text) => console.log("test")}
        autoCorrect={false}
      />
    );
  };

  return loading === false ? (
    <LoadingIcon />
  ) : (
    <SafeAreaView style={styles.container}>
      <FlatList
        extraData={selectedId}
        renderItem={renderItem}
        keyExtractor={(item) => item.userResponse.id.toString()}
        data={following}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
