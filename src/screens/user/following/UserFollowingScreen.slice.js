import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getUserFollowing } from "../../../api/user/getUserFollowing";

export const getUserFollowingList = createAsyncThunk(
  "getUserFollowingList",
  async (id) => await getUserFollowing(id)
);

export const initialState = {
  following: [],
  error: null,
};

export const userFollowingListSlice = createSlice({
  name: "userFollowingListSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserFollowingList.fulfilled, (state, action) => {
      console.log("reducer", action);
      state.following = action.payload;
    });
  },
});

const userFollowingList = userFollowingListSlice.reducer;

export default userFollowingList;
