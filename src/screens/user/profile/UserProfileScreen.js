import * as React from "react";

import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";

import { useDispatch, useSelector } from "react-redux";
import { Avatar, Button } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";

import * as Constants from "../../../Constants";

import {
  getAllUserAdvertisement,
  getUserDetails,
} from "./UserProfileScreen.slice";

import AllAdvertisements from "../../../components/AllAdvertisements";

import { followUser } from "../../../api/user/followUser";
import { unfollowUser } from "../../../api/user/unfollowUser";
import { getExpoPushToken } from "../../../api/expo-notifications/getExpoPushToken";
import { sendPushNotification } from "../../../components/hooks/sendPushNotification";
import { getCurrentUser } from "../../../api/security/auth";

export default function UserProfileScreen({ route, navigation }) {
  const { id } = route.params;
  const dispatch = useDispatch();
  const [followButtonStatusChange, setFollowButtonStatusChange] =
    React.useState(false);
  const [loading, setFinishedLoading] = React.useState(false);
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  const onUnfollow = async () => {
    await unfollowUser(id);
    setFollowButtonStatusChange(!followButtonStatusChange);
  };

  const onFollow = async () => {
    await followUser(id);

    const currentUser = await getCurrentUser();
    const token = await getExpoPushToken(id);
    await sendPushNotification(
      token.expoPushToken,
      "New follower!",
      currentUser.fullName + " just followed you!"
    );
    setFollowButtonStatusChange(!followButtonStatusChange);
  };

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchUserData() {
      await dispatch(getUserDetails(id));
      await dispatch(getAllUserAdvertisement(id));
      setFinishedLoading(true);
    }

    fetchUserData();
  }, [dispatch, id, followButtonStatusChange, refreshToggle]);

  const { user, advertisements } = useSelector((state) => state.userDetails);

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <View
        style={{
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Avatar.Image
          style={{ marginTop: 10 }}
          size={80}
          source={require("../../assets/avatar.png")}
        />
        <Text
          style={{
            fontSize: 18,
            marginTop: 4,
            fontWeight: "bold",
          }}
        >
          {user.fullName}
        </Text>
        <View
          style={{
            flexDirection: "row",
            marginTop: 8,
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginRight: 20,
            }}
            onPress={() =>
              navigation.navigate("UserFollowingScreen", { id: id })
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Following
            </Text>
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
              }}
            >
              {user.totalFollowingNumber}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginRight: 20,
            }}
            onPress={() =>
              navigation.navigate("UserFollowersScreen", { id: id })
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Followers
            </Text>
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
              }}
            >
              {user.totalFollowersNumber}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
            }}
            onPress={() =>
              user.totalRatingScore !== "0"
                ? navigation.navigate("UserRatingScreen", { id: user.id })
                : null
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Rating
            </Text>
            <View
              style={{
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  marginTop: 4,
                  marginRight: 2,
                }}
              >
                {user.totalRatingScore}
              </Text>
              <Ionicons name="star" size={22} color="yellow" />
            </View>
          </TouchableOpacity>
        </View>
        {user.isFollowed ? (
          <Button
            style={{ margin: 10 }}
            mode="contained"
            onPress={() => onUnfollow()}
            contentStyle={{}}
          >
            Unfollow
            <Ionicons name="person-remove" size={20} color="white" />
          </Button>
        ) : (
          <Button
            style={{ margin: 10 }}
            mode="contained"
            onPress={() => onFollow()}
          >
            Follow
            <Ionicons name="person-add" size={20} color="white" />
          </Button>
        )}
      </View>
      <AllAdvertisements
        advertisements={advertisements}
        navigation={navigation}
        onRefresh={() => onRefreshData()}
        refresh={refresh}
      />
    </View>
  );
}
