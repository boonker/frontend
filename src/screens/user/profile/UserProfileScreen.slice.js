import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getUser } from "../../../api/user/getUser";
import { getUserAdvertisement } from "../../../api/user/getUserAdvertisement";

export const getUserDetails = createAsyncThunk(
  "getUserDetails",
  async (id) => await getUser(id)
);

export const getAllUserAdvertisement = createAsyncThunk(
  "getAllUserAdvertisement",
  async (id) => await getUserAdvertisement(id)
);

export const initialState = {
  user: {},
  advertisements: [],
  error: null,
};

export const userProfileScreenSlice = createSlice({
  name: "userProfileScreenSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserDetails.fulfilled, (state, action) => {
      state.user = action.payload;
    });
    builder.addCase(getAllUserAdvertisement.fulfilled, (state, action) => {
      state.advertisements = action.payload;
    });
  },
});

const userProfileScreen = userProfileScreenSlice.reducer;

export default userProfileScreen;
