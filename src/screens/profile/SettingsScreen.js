import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as Constants from "../../Constants";
import { Avatar, Button, TextInput } from "react-native-paper";
import { getUserPersonalInfo } from "./SettingsScreen.slice";
import { Formik } from "formik";
import ErrorField from "../../components/common/ErrorField";
import * as Yup from "yup";
import { updatePersonalInfo } from "../../api/user/updatePersonalInfo";

const UpdatePersonalInfo = Yup.object().shape({
  fullName: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  city: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  age: Yup.number()
    .min(18, "Sorry! You should be at least 18 years old!")
    .required("Required!"),
  phoneNumber: Yup.string().required("Required!"),
});

export default function SettingsScreen() {
  const dispatch = useDispatch();
  const [loading, setFinishedLoading] = React.useState(false);
  const [secureEntry, setSecureEntry] = React.useState(true);

  const onEyePress = () => {
    setSecureEntry(!secureEntry);
  };
  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchPersonalInfo() {
      await dispatch(getUserPersonalInfo());
      setFinishedLoading(true);
    }

    fetchPersonalInfo();
  }, [dispatch]);

  const { personalInfo } = useSelector((state) => state.personalInfo);

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <View
        style={{
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Avatar.Image
          style={{ marginTop: 10 }}
          size={80}
          source={require("../../screens/assets/avatar.png")}
        />
        <Text
          style={{
            fontSize: 18,
            marginTop: 4,
            fontWeight: "bold",
          }}
        >
          {personalInfo.fullName}
        </Text>
      </View>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          justifyContent: "center",
          paddingVertical: 10,
        }}
      >
        <Formik
          initialValues={{
            fullName: personalInfo.fullName,
            phoneNumber: personalInfo.phoneNumber,
            age: personalInfo.age,
            password: "",
            city: personalInfo.city,
          }}
          validationSchema={UpdatePersonalInfo}
          onSubmit={async (values, { resetForm }) => {
            await updatePersonalInfo(values);
            Alert.alert(
              "Update profile",
              "You successfully updated your profile"
            );
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <SafeAreaView
              style={{
                alignItems: "center",
              }}
            >
              <SafeAreaView style={style.textInputView}>
                {errors.fullName && touched.fullName ? (
                  <ErrorField errorText={errors.fullName} />
                ) : null}
                <TextInput
                  value={values.fullName}
                  placeholder={"Full name"}
                  onChangeText={handleChange("fullName")}
                  onBlur={handleBlur("fullName")}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.age && touched.age ? (
                  <ErrorField errorText={errors.age} />
                ) : null}
                <TextInput
                  value={values.age}
                  placeholder={"Age"}
                  onChangeText={handleChange("age")}
                  onBlur={handleBlur("age")}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.city && touched.city ? (
                  <ErrorField errorText={errors.city} />
                ) : null}
                <TextInput
                  value={values.city}
                  placeholder={"Age"}
                  onChangeText={handleChange("city")}
                  onBlur={handleBlur("city")}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.phoneNumber && touched.phoneNumber ? (
                  <ErrorField errorText={errors.phoneNumber} />
                ) : null}
                <TextInput
                  value={values.phoneNumber}
                  placeholder={"phoneNumber"}
                  onChangeText={handleChange("phoneNumber")}
                  onBlur={handleBlur("phoneNumber")}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                <TextInput
                  value={personalInfo.email}
                  placeholder={"email"}
                  disabled={true}
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 12,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView>
                {errors.password && touched.password ? (
                  <ErrorField errorText={errors.password} />
                ) : null}
                <TextInput
                  value={values.password}
                  placeholder={"New password"}
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  secureTextEntry={secureEntry}
                  underlineColor={"transparent"}
                  right={
                    <TextInput.Icon
                      name="eye"
                      color={Constants.PRIMARY_COLOR}
                      onPress={onEyePress}
                    />
                  }
                  style={{
                    height: 40,
                    width: 300,
                    marginBottom: 20,
                    borderRadius: 20,
                    overflow: "hidden",
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                  }}
                  name={"password"}
                />
              </SafeAreaView>
              <Button
                onPress={handleSubmit}
                mode="contained"
                contentStyle={{
                  width: 200,
                }}
              >
                Update profile info
              </Button>
            </SafeAreaView>
          )}
        </Formik>
      </ScrollView>
    </View>
  );
}

const style = StyleSheet.create({
  textInputStyle: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textInputView: { alignItems: "flex-start" },
});
