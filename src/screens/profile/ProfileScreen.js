import * as React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import AuthService from "../../api/security/auth";

import * as Constants from "../../Constants";

import MyProfileScreen from "./MyProfileScreen";
import SettingsScreen from "./SettingsScreen";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

export default function ProfileScreen({ route, navigation }) {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        component={MyProfileScreen}
        name={"MyProfileScreen"}
        options={{
          headerShown: true,
          title: "Boonker",
          headerLeft: null,
          headerTitleStyle: { alignSelf: "center" },
          headerRight: () => (
            <View style={{ flexDirection: "row" }}>
              <Ionicons
                name="settings"
                size={24}
                color={Constants.PRIMARY_COLOR}
                style={{ marginRight: 6 }}
                onPress={() => navigation.navigate("SettingsScreen")}
              />
              <MaterialIcons
                name="logout"
                size={24}
                color={Constants.PRIMARY_COLOR}
                style={{
                  marginRight: 6,
                }}
                onPress={async () => {
                  await AuthService.logout();
                  navigation.navigate("LoginScreen");
                }}
              />
            </View>
          ),
        }}
      />
      <Stack.Screen
        component={SettingsScreen}
        name={"SettingsScreen"}
        options={{
          headerShown: true,
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
    </Stack.Navigator>
  );
}
