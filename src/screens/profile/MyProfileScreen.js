import * as React from "react";

import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import * as Constants from "../../Constants";

import {
  getAllUserAdvertisement,
  getUserDetails,
} from "../user/profile/UserProfileScreen.slice";

import { Avatar } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import AllAdvertisements from "../../components/AllAdvertisements";
import { getCurrentUser } from "../../api/security/auth";

export default function MyProfileScreen({ navigation }) {
  const dispatch = useDispatch();
  const [loading, setFinishedLoading] = React.useState(false);
  const [currentUserId, setCurrentUserId] = React.useState();

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchUserData() {
      var user = await getCurrentUser();
      setCurrentUserId(user.id);

      await dispatch(getUserDetails(user.id));
      await dispatch(getAllUserAdvertisement(user.id));
      setFinishedLoading(true);
    }

    fetchUserData();
  }, [dispatch]);

  const { user, advertisements } = useSelector((state) => state.userDetails);

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <View
        style={{
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Avatar.Image
          style={{ marginTop: 10 }}
          size={80}
          source={require("../../screens/assets/avatar.png")}
        />
        <Text
          style={{
            fontSize: 18,
            marginTop: 4,
            fontWeight: "bold",
          }}
        >
          {user.fullName}
        </Text>
        <View
          style={{
            flexDirection: "row",
            marginTop: 8,
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginRight: 20,
            }}
            onPress={() =>
              navigation.navigate("UserFollowingScreen", { id: currentUserId })
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Following
            </Text>
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
              }}
            >
              {user.totalFollowingNumber}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
              marginRight: 20,
            }}
            onPress={() =>
              navigation.navigate("UserFollowersScreen", { id: currentUserId })
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Followers
            </Text>
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
              }}
            >
              {user.totalFollowersNumber}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
            }}
            onPress={() =>
              user.totalRatingScore !== "0"
                ? navigation.navigate("UserRatingScreen", { id: currentUserId })
                : null
            }
          >
            <Text
              style={{
                fontSize: 14,
                marginTop: 4,
                fontWeight: "bold",
              }}
            >
              Rating
            </Text>
            <View
              style={{
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  marginTop: 4,
                  marginRight: 2,
                }}
              >
                {user.totalRatingScore}
              </Text>
              <Ionicons name="star" size={22} color="yellow" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <AllAdvertisements
        advertisements={advertisements}
        navigation={navigation}
      />
    </View>
  );
}
