import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getPersonalInfo } from "../../api/user/getPersonalInfo";

export const getUserPersonalInfo = createAsyncThunk(
  "getUserPersonalInfo",
  async () => await getPersonalInfo()
);

export const initialState = {
  personalInfo: {},
};

export const personalInfoSlice = createSlice({
  name: "personalInfoSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserPersonalInfo.fulfilled, (state, action) => {
      console.log(action.payload);
      state.personalInfo = action.payload;
    });
  },
});

const personalInfo = personalInfoSlice.reducer;

export default personalInfo;
