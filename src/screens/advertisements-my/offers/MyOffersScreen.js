import * as React from "react";

import { useDispatch, useSelector } from "react-redux";

import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import { Avatar, Card, Text } from "react-native-paper";

import { getAllUserOffers } from "./MyOffersScreen.slice";
import { getOneAdvertisement } from "../../advertisements-update/UpdateAdvertisementsScreen.slice";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import * as Constants from "../../../Constants";
import LoadingIcon from "../../../components/common/LoadingIcon";

const RightContent = ({ type }) => (
  <Text style={{ marginRight: 8 }}>{type}</Text>
);
const LeftContent = () => (
  <Avatar.Image size={48} source={require("../../assets/avatar.png")} />
);

const Item = ({ item, navigation }) => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getOneAdvertisement(item.advertisementId));
  }, [dispatch]);

  const { advertisement } = useSelector((state) => state.updateAdvertisement);

  return (
    <TouchableOpacity
      style={{
        margin: 4,
      }}
      onPress={() =>
        navigation.navigate("OneAdvertisementScreen", {
          id: item.advertisementId,
          userId: advertisement.userId,
        })
      }
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <Card.Title
          title={advertisement.title}
          subtitle={advertisement.userFullName}
          right={() => <RightContent type={item.offerType} />}
          left={LeftContent}
        />
        <Card.Content>
          <View style={{ flexDirection: "row", marginLeft: 52 }}>
            <Ionicons
              name={"location"}
              size={14}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 12,
                justifyContent: "space-between",
                marginLeft: 4,
                marginRight: 4,
              }}
            >
              {advertisement.cityFrom}, {advertisement.cityFromAddress}
            </Text>
            <Ionicons name="car" size={14} color={Constants.PRIMARY_COLOR} />
            <Text
              style={{
                fontSize: 12,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              {advertisement.deliveryDate}
            </Text>
          </View>
          <View
            style={{
              marginTop: 4,
              borderBottomColor: Constants.PRIMARY_COLOR,
              borderBottomWidth: 1,
            }}
          />
          <View style={{ flexDirection: "row", marginLeft: 52, marginTop: 4 }}>
            <Ionicons
              name={"location"}
              size={14}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 12,
                justifyContent: "space-between",
                marginLeft: 4,
                marginRight: 4,
              }}
            >
              {advertisement.cityTo}, {item.address}
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 4, marginLeft: 52 }}>
            <Ionicons
              name={"document-text"}
              size={14}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 12,
                justifyContent: "space-between",
                marginLeft: 4,
                marginRight: 4,
              }}
            >
              {item.message}
            </Text>
          </View>
        </Card.Content>
      </Card>
    </TouchableOpacity>
  );
};

export default function MyOffersScreen({ navigation }) {
  const dispatch = useDispatch();

  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);
  const [loadingData, setLoadingData] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  React.useEffect(() => {
    setLoadingData(false);
    dispatch(getAllUserOffers());
    setLoadingData(true);
  }, [dispatch, refreshToggle]);

  const content = useSelector((state) => state.userOffers);

  const renderItem = ({ item }) => <Item item={item} navigation={navigation} />;
  return loadingData === false ? (
    <LoadingIcon />
  ) : (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={content.offers}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        onRefresh={() => onRefreshData()}
        refreshing={refresh}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
