import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getUserOffers } from "../../../api/offer/getUserOffers";

export const getAllUserOffers = createAsyncThunk(
  "getAllUserOffers",
  getUserOffers
);

export const initialState = {
  offers: [],
  loading: false,
  error: null,
};

export const userOffersSlice = createSlice({
  name: "userOffersSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllUserOffers.fulfilled, (state, action) => {
      state.offers = action.payload;
    });
  },
});

const userOffers = userOffersSlice.reducer;

export default userOffers;
