import * as React from "react";

import { Dimensions } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import AdvertisementsScreen from "./advertisements/AdvertisementsScreen";
import MyOffersScreen from "./offers/MyOffersScreen";
import UpdateAdvertisementsScreen from "../advertisements-update/UpdateAdvertisementsScreen";
import AdvertisementOffersScreen from "../offer/AdvertisementOffersScreen";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

function MyAdvertisementsTabs() {
  return (
    <Tab.Navigator
      initialLayout={{
        width: Dimensions.get("window").width - 20,
        height: Dimensions.get("window").height - 20,
        margin: 40,
      }}
    >
      <Tab.Screen name="My ads" component={AdvertisementsScreen} />
      <Tab.Screen name="My offers" component={MyOffersScreen} />
    </Tab.Navigator>
  );
}

export default function MyAdvertisementsNavigationScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={"MyAdvertisementsTabs"}
        component={MyAdvertisementsTabs}
        options={{
          title: "Boonker",
          headerLeft: null,
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name="UpdateAdvertisements"
        component={UpdateAdvertisementsScreen}
        options={{ title: "Update ad" }}
      />
      <Stack.Screen
        name="AdvertisementOffersScreen"
        component={AdvertisementOffersScreen}
        options={{ title: "View offers" }}
      />
    </Stack.Navigator>
  );
}
