import { allMyAdvertisements } from "../../../api/advertisement/allMyAdvertisements";

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getAllMyAdvertisements = createAsyncThunk(
  "getAllMyAdvertisements",
  allMyAdvertisements
);

export const initialState = {
  advertisements: [],
  loading: false,
  error: null,
};

export const advertisementsSlice = createSlice({
  name: "advertisementsSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllMyAdvertisements.fulfilled, (state, action) => {
      state.advertisements = action.payload;
    });
  },
});

const advertisements = advertisementsSlice.reducer;

export default advertisements;
