import * as React from "react";

import { useDispatch, useSelector } from "react-redux";

import {
  Alert,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { Avatar, Card, Paragraph, Text } from "react-native-paper";

import * as Constants from "../../../Constants";

import { deleteAdvertisement } from "../../../api/advertisement/deleteAdvertisement";
import { getAllMyAdvertisements } from "./AdvertisementsScreen.slice";
import LoadingIcon from "../../../components/common/LoadingIcon";

const LeftContent = (props) => (
  <Avatar.Icon style={{ margin: 10 }} {...props} icon="check" />
);

const Item = ({ item, navigation }) => {
  const dispatch = useDispatch();

  const onDelete = async () => {
    Alert.alert(
      "Delete this advertisement",
      "Are you sure? All pending offers will be canceled!",
      [
        {
          text: "Yes",
          onPress: async () => {
            await deleteAdvertisement(item.id);
            dispatch(getAllMyAdvertisements());
          },
        },
        { text: "Cancel" },
      ]
    );
  };

  const onUpdate = () => {
    navigation.navigate("UpdateAdvertisements", { id: item.id });
  };

  return (
    <TouchableOpacity
      style={{
        margin: 4,
      }}
      onPress={() =>
        navigation.navigate("AdvertisementOffersScreen", { id: item.id })
      }
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <Card.Title title={item.title} right={LeftContent} />
        <Card.Content>
          <View
            style={{
              justifyContent: "space-between",
              flexDirection: "row",
              marginTop: -18,
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "baseline",
              }}
            >
              <Text
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
                style={styles.info}
              >
                {item.cityFrom}, {item.cityFromAddress}
              </Text>
              <Ionicons
                name="arrow-forward"
                size={16}
                color={Constants.PRIMARY_COLOR}
              />
              <Text
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
                style={styles.info}
              >
                {item.cityTo}, {item.cityToAddress}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ marginRight: 2 }}>{item.price}kn,</Text>
            <Text>{item.packageSize}kg</Text>
          </View>
          <Text
            style={{
              fontSize: 12,
              color: "gray",
              marginLeft: 1,
              marginTop: 1,
            }}
          >
            Delivery date: {item.deliveryDate}
          </Text>
          <Paragraph>{item.content}</Paragraph>
        </Card.Content>
        <Card.Cover
          source={{
            uri: "https://icdn.digitaltrends.com/image/digitaltrends/amazon-package-2-720x720.jpg",
          }}
        />
        <Card.Actions>
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              justifyContent: "flex-end",
              alignItems: "baseline",
            }}
          >
            <Ionicons
              name="pencil"
              size={24}
              color="#6200ee"
              onPress={onUpdate}
            />
            <Ionicons
              name="trash"
              size={24}
              color="#6200ee"
              style={{
                marginLeft: 10,
              }}
              onPress={onDelete}
            />
          </View>
        </Card.Actions>
      </Card>
    </TouchableOpacity>
  );
};

export default function AdvertisementsScreen({ navigation }) {
  const dispatch = useDispatch();

  const [loadingData, setLoadingData] = React.useState(false);
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  React.useEffect(() => {
    setLoadingData(false);
    dispatch(getAllMyAdvertisements());
    setLoadingData(true);
  }, [dispatch, refreshToggle]);

  const { advertisements } = useSelector((state) => state.content);

  const renderItem = ({ item }) => <Item item={item} navigation={navigation} />;

  return loadingData === false ? (
    <LoadingIcon />
  ) : (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={advertisements}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        refreshing={refresh}
        onRefresh={onRefreshData}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  info: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
