import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getAdvertisement } from "../../api/advertisement/getAdvertisement";

export const getOneAdvertisement = createAsyncThunk(
  "getOneAdvertisement",
  async (id) => await getAdvertisement(id)
);

export const initialState = {
  advertisement: {},
  error: null,
};

export const updateAdvertisementSlice = createSlice({
  name: "updateAdvertisementSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    console.log("builder", builder);
    builder.addCase(getOneAdvertisement.fulfilled, (state, action) => {
      state.advertisement = action.payload;
    });
  },
});

const updateAdvertisement = updateAdvertisementSlice.reducer;

export default updateAdvertisement;
