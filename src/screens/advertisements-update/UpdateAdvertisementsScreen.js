import * as React from "react";

import * as Constants from "../../Constants";

import DateTimePicker from "@react-native-community/datetimepicker";
import { Formik } from "formik";
import { Ionicons } from "@expo/vector-icons";
import {
  Alert,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import { getAllMyAdvertisements } from "../advertisements-my/advertisements/AdvertisementsScreen.slice";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import { getOneAdvertisement } from "./UpdateAdvertisementsScreen.slice";
import { Button, TextInput } from "react-native-paper";
import { updateAdvertisement } from "../../api/advertisement/updateAdvertisments";
import ErrorField from "../../components/common/ErrorField";

const UpdateAdvertisementSchema = Yup.object().shape({
  title: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  content: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(200, "Too song! Should be 200 chars max")
    .required("Required!"),
  price: Yup.number().required("Required!").nullable(),
  deliveryDate: Yup.string().required("Required!").nullable(),
  cityFrom: Yup.string().required("Required!").nullable(),
  cityTo: Yup.string().required("Required!").nullable(),
  packageSize: Yup.number().required("Required!").nullable(),
});

export default function UpdateAdvertisementsScreen({ route, navigation }) {
  const { id } = route.params;
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getOneAdvertisement(id));
  }, [dispatch]);

  const oneAdvertisement = useSelector(
    (state) => state.updateAdvertisement.advertisement
  );

  const [deliveryDate, setDeliveryDate] = React.useState(new Date());
  const [show, setShow] = React.useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || expectedDeliveryDate;
    setShow(Platform.OS === "ios");
    setDeliveryDate(currentDate);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: "center",
        paddingVertical: 10,
      }}
    >
      <Formik
        enableReinitialize={true}
        initialValues={{
          title: oneAdvertisement.title,
          content: oneAdvertisement.content,
          price: oneAdvertisement.price
            ? oneAdvertisement.price.toString()
            : "",
          deliveryDate: deliveryDate.toLocaleDateString(),
          packageSize: oneAdvertisement.packageSize,
          cityFrom: oneAdvertisement.cityFrom,
          cityTo: oneAdvertisement.cityTo,
        }}
        onSubmit={async (values, { resetForm }) => {
          await updateAdvertisement(id, values);

          dispatch(getOneAdvertisement(id));
          dispatch(getAllMyAdvertisements());

          Alert.alert(
            "Updated advertisement",
            "You successfully updated your advertisement"
          );
        }}
        validationSchema={UpdateAdvertisementSchema}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
          setFieldValue,
        }) => (
          <SafeAreaView
            style={{
              alignItems: "center",
            }}
          >
            <SafeAreaView style={style.textInputView}>
              {errors.title && touched.title ? (
                <ErrorField errorText={errors.title} />
              ) : null}
              <TextInput
                value={values.title}
                placeholder={"Title"}
                onChangeText={handleChange("title")}
                onBlur={handleBlur("title")}
                style={{
                  height: 40,
                  width: 300,
                  marginBottom: 12,
                  borderRadius: 20,
                  overflow: "hidden",
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}
                underlineColor={"transparent"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.content && touched.content ? (
                <ErrorField errorText={errors.content} />
              ) : null}
              <TextInput
                value={values.content}
                placeholder={"Content"}
                multiline={true}
                numberOfLines={4}
                onChangeText={handleChange("content")}
                onBlur={handleBlur("content")}
                style={{
                  width: 300,
                  marginBottom: 12,
                  borderRadius: 20,
                  overflow: "hidden",
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}
                underlineColor={"transparent"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.price && touched.price ? (
                <ErrorField errorText={errors.price} />
              ) : null}
              <TextInput
                value={values.price}
                placeholder={"Price (kn)"}
                keyboardType="numeric"
                onChangeText={handleChange("price")}
                onBlur={handleBlur("price")}
                style={style.textInputStyle}
                underlineColor={"transparent"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.cityFrom && touched.cityFrom ? (
                <ErrorField errorText={errors.cityFrom} />
              ) : null}
              <TextInput
                value={values.cityFrom}
                placeholder={"From"}
                onChangeText={handleChange("cityFrom")}
                onBlur={handleBlur("cityFrom")}
                style={style.textInputStyle}
                underlineColor={"transparent"}
                name={"from"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.cityTo && touched.cityTo ? (
                <ErrorField errorText={errors.cityTo} />
              ) : null}
              <TextInput
                value={values.cityTo}
                placeholder={"To"}
                onChangeText={handleChange("cityTo")}
                onBlur={handleBlur("cityTo")}
                style={style.textInputStyle}
                underlineColor={"transparent"}
                name={"to"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.packageSize && touched.packageSize ? (
                <ErrorField errorText={errors.packageSize} />
              ) : null}
              <TextInput
                value={values.packageSize}
                placeholder={"Package size"}
                onChangeText={handleChange("packageSize")}
                onBlur={handleBlur("packageSize")}
                style={style.textInputStyle}
                underlineColor={"transparent"}
                name={"packageSize"}
              />
            </SafeAreaView>
            <SafeAreaView style={style.textInputView}>
              {errors.packageSize && touched.packageSize ? (
                <ErrorField errorText={errors.packageSize} />
              ) : null}
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  width: 300,
                  marginBottom: 30,
                }}
              >
                <TextInput
                  value={values.deliveryDate}
                  placeholder={"Expected delivery date"}
                  editable={false}
                  onChangeText={handleChange("date")}
                  onBlur={handleBlur("date")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />

                <TouchableOpacity
                  style={{
                    height: 60,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onPress={showDatepicker}
                >
                  <Ionicons
                    name={"calendar"}
                    size={24}
                    color={Constants.PRIMARY_COLOR}
                    onPress={showDatepicker}
                    style={{
                      marginLeft: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={deliveryDate}
                  mode={"date"}
                  display="default"
                  onChange={(event, selectedDate) => {
                    onChange(event, selectedDate);
                    setFieldValue(
                      "deliveryDate",
                      selectedDate
                        .toISOString()
                        .replace(/T.*/, "")
                        .split("-")
                        .join("-")
                    );
                  }}
                />
              )}
            </SafeAreaView>
            <Button
              onPress={handleSubmit}
              mode="contained"
              contentStyle={{
                width: 200,
              }}
            >
              Submit
            </Button>
          </SafeAreaView>
        )}
      </Formik>
    </ScrollView>
  );
}

const style = StyleSheet.create({
  textInputStyle: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textInputView: { alignItems: "flex-start" },
});
