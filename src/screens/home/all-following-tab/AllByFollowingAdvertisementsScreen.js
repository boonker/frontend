import * as React from "react";

import { useDispatch, useSelector } from "react-redux";
import AllAdvertisements from "../../../components/AllAdvertisements";
import LoadingIcon from "../../../components/common/LoadingIcon";
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Button, Chip, Searchbar, TextInput } from "react-native-paper";
import { Formik } from "formik";
import SelectDropdown from "react-native-select-dropdown";
import { Ionicons } from "@expo/vector-icons";
import * as Constants from "../../../Constants";
import DateTimePicker from "@react-native-community/datetimepicker";
import { getAllFollowingAdvertisements } from "./AllByFollowingAdvertisements.slice";

export default function AllByFollowingAdvertisementsScreen({ navigation }) {
  const advertisementTypes = ["DRIVING", "SENDING"];
  const [loading, setFinishedLoading] = React.useState(false);
  const [showChip, setShowChip] = React.useState(false);
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    console.log(refresh);
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  const initialSearchQueryState = {
    cityFrom: "",
    cityTo: "",
    sortBy: "",
    deliveryDate: "",
    advertisementType: "",
    followedByUser: true,
    userFullName: "",
  };
  const [searchQuery, setSearchQuery] = React.useState(initialSearchQueryState);
  const [selectedId, setSelectedId] = React.useState();

  const [viewSearchForm, setViewSearchForm] = React.useState(false);
  const [selectedAdvertisementType, setSelectedAdvertisementType] =
    React.useState("");
  const [selectedPrice, setSelectedPrice] = React.useState("");

  const dispatch = useDispatch();

  const onSearchFilterReset = () => {
    setSearchQuery(initialSearchQueryState);
    setShowChip(false);
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || deliveryDate;
    setShow(Platform.OS === "ios");
    setDeliveryDate(currentDate);
  };

  React.useEffect(() => {
    setFinishedLoading(false);
    async function fetchAds() {
      await dispatch(getAllFollowingAdvertisements(searchQuery));

      setFinishedLoading(true);
      setSelectedId(Math.random());
      setViewSearchForm(false);
    }

    fetchAds();
  }, [dispatch, searchQuery, refreshToggle]);

  const { advertisements } = useSelector(
    (state) => state.followingAdvertisements
  );

  const [deliveryDate, setDeliveryDate] = React.useState(new Date());
  const [show, setShow] = React.useState(false);

  const showDatepicker = () => {
    setShow(true);
  };

  return loading === false ? (
    <LoadingIcon />
  ) : (
    <View>
      <TouchableOpacity
        onPress={() => setViewSearchForm(true)}
        activeOpacity={0.3}
      >
        <Searchbar
          placeholder="Search"
          value={""}
          style={{
            marginRight: 4,
            marginLeft: 4,
            marginTop: 12,
            borderRadius: 20,
          }}
          editable={false}
        />
      </TouchableOpacity>
      {showChip ? (
        <Chip
          icon="close"
          onPress={onSearchFilterReset}
          style={{ marginTop: 12, marginBottom: -2, marginLeft: 4, width: 155 }}
          mode="outlined"
          selectedColor={"red"}
        >
          Clear search filter
        </Chip>
      ) : null}
      {viewSearchForm ? (
        <View>
          <Formik
            initialValues={{
              cityFrom: "",
              cityTo: "",
              sortBy: "LOW_TO_HIGH_PRICE",
              advertisementType: "",
              deliveryDate: "",
              userFullName: "",
            }}
            onSubmit={async (values, { resetForm }) => {
              setSearchQuery({
                cityFrom: values.cityFrom,
                cityTo: values.cityTo,
                sortBy: selectedPrice,
                deliveryDate: values.deliveryDate,
                advertisementType: selectedAdvertisementType,
                followedByUser: true,
                userFullName: values.userFullName,
              });
              setShowChip(true);
              resetForm();
            }}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              setFieldValue,
            }) => (
              <SafeAreaView
                style={{
                  alignItems: "center",
                }}
              >
                <SafeAreaView style={{ ...style.textInputView, marginTop: 14 }}>
                  <TextInput
                    value={values.userFullName}
                    placeholder={"User full name"}
                    onChangeText={handleChange("userFullName")}
                    onBlur={handleBlur("userFullName")}
                    style={style.textInputStyle}
                    underlineColor={"transparent"}
                  />
                </SafeAreaView>
                <SafeAreaView style={{ ...style.textInputView, marginTop: 14 }}>
                  <TextInput
                    value={values.cityFrom}
                    placeholder={"From city"}
                    onChangeText={handleChange("cityFrom")}
                    onBlur={handleBlur("cityFrom")}
                    style={style.textInputStyle}
                    underlineColor={"transparent"}
                  />
                </SafeAreaView>
                <SafeAreaView style={style.textInputView}>
                  <TextInput
                    value={values.cityTo}
                    placeholder={"To city"}
                    onChangeText={handleChange("cityTo")}
                    onBlur={handleBlur("cityTo")}
                    style={style.textInputStyle}
                    underlineColor={"transparent"}
                  />
                </SafeAreaView>
                <SafeAreaView style={style.textInputView}>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between",
                      width: 300,
                    }}
                  >
                    <TextInput
                      value={values.deliveryDate}
                      placeholder={"Delivery date"}
                      editable={false}
                      onChangeText={handleChange("date")}
                      onBlur={handleBlur("date")}
                      style={style.textInputStyle}
                      underlineColor={"transparent"}
                      right={
                        values.deliveryDate !== "" ? (
                          <TextInput.Icon
                            name={"close"}
                            color={"gray"}
                            onPress={() => setFieldValue("deliveryDate", "")}
                          />
                        ) : null
                      }
                    />

                    <TouchableOpacity
                      style={{
                        height: 60,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      onPress={showDatepicker}
                    >
                      <Ionicons
                        name={"calendar"}
                        size={24}
                        color={Constants.PRIMARY_COLOR}
                        onPress={showDatepicker}
                        style={{
                          marginLeft: 4,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  {show && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      value={deliveryDate}
                      mode={"date"}
                      display="default"
                      onChange={(event, selectedDate) => {
                        onChange(event, selectedDate);
                        setFieldValue(
                          "deliveryDate",
                          selectedDate
                            .toISOString()
                            .replace(/T.*/, "")
                            .split("-")
                            .join("-")
                        );
                      }}
                    />
                  )}
                </SafeAreaView>
                <SafeAreaView
                  style={{
                    alignItems: "flex-start",
                    width: 300,
                    ...style.textInputStyle,
                  }}
                >
                  <SelectDropdown
                    buttonStyle={{
                      width: 200,
                      height: 40,
                      borderRadius: 20,
                      alignItems: "center",
                    }}
                    buttonTextStyle={{
                      fontSize: 16,
                      color: "gray",
                    }}
                    defaultButtonText={"Advertisement type"}
                    data={advertisementTypes}
                    onSelect={(selectedItem) =>
                      setSelectedAdvertisementType(selectedItem)
                    }
                    dropdownStyle={{
                      borderRadius: 20,
                    }}
                  />
                </SafeAreaView>
                <SafeAreaView
                  style={{
                    alignItems: "flex-start",
                    width: 300,
                    marginBottom: 12,
                  }}
                >
                  <SelectDropdown
                    buttonStyle={{
                      width: 200,
                      height: 40,
                      borderRadius: 20,
                      alignItems: "center",
                    }}
                    buttonTextStyle={{
                      fontSize: 16,
                      color: "gray",
                    }}
                    defaultButtonText={"Sort by price"}
                    data={["Price low to high", "Price high to low"]}
                    onSelect={(selectedItem) => {
                      if (selectedItem === "Price low to high") {
                        setSelectedPrice("LOW_TO_HIGH_PRICE");
                      } else {
                        setSelectedPrice("HIGH_TO_LOW_PRICE");
                      }
                    }}
                    dropdownStyle={{
                      borderRadius: 20,
                    }}
                    buttonTextAfterSelection={(selectedItem) => {
                      return selectedItem;
                    }}
                    rowTextForSelection={(item) => {
                      return item;
                    }}
                  />
                </SafeAreaView>
                <View style={{ flexDirection: "row" }}>
                  <Button
                    onPress={handleSubmit}
                    style={{ width: 150, marginRight: 2 }}
                    mode="contained"
                  >
                    Filter
                  </Button>
                  <Button
                    onPress={() => setViewSearchForm(false)}
                    style={{ width: 150, marginLeft: 2 }}
                    mode={"contained"}
                    labelStyle={{
                      color: "white",
                    }}
                    color={Constants.SECONDARY_COLOR}
                  >
                    Cancel
                  </Button>
                </View>
              </SafeAreaView>
            )}
          </Formik>
        </View>
      ) : (
        <AllAdvertisements
          advertisements={advertisements}
          extraData={selectedId}
          navigation={navigation}
          onRefresh={() => onRefreshData()}
          refresh={refresh}
        />
      )}
    </View>
  );
}

const style = StyleSheet.create({
  textInputStyle: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textInputView: { alignItems: "flex-start" },
});
