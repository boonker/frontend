import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { allAdvertisements } from "../../../api/advertisement/allAdvertisements";

export const getAllFollowingAdvertisements = createAsyncThunk(
  "getAllFollowingAdvertisements",
  async (searchAdvertisementQueryRequest) =>
    await allAdvertisements(searchAdvertisementQueryRequest)
);

export const initialState = {
  advertisements: [],
  loading: false,
  error: null,
};

export const getAllFollowingAdvertisementsSlice = createSlice({
  name: "getAllFollowingAdvertisementsSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      getAllFollowingAdvertisements.fulfilled,
      (state, action) => {
        state.advertisements = action.payload;
      }
    );
  },
});

const followingAdvertisements = getAllFollowingAdvertisementsSlice.reducer;

export default followingAdvertisements;
