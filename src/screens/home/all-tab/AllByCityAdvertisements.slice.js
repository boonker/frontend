import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { allAdvertisements } from "../../../api/advertisement/allAdvertisements";

export const getAllAdvertisements = createAsyncThunk(
  "getAllAdvertisements",
  async (searchAdvertisementQueryRequest) =>
    await allAdvertisements(searchAdvertisementQueryRequest)
);

export const initialState = {
  advertisements: [],
  loading: false,
  error: null,
};

export const getAllAdvertisementsSlice = createSlice({
  name: "getAllAdvertisementsSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllAdvertisements.fulfilled, (state, action) => {
      state.advertisements = action.payload;
    });
  },
});

const cityAdvertisements = getAllAdvertisementsSlice.reducer;

export default cityAdvertisements;
