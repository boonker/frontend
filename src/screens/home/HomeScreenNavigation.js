import * as React from "react";

import { Dimensions, Text, View } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import AllByCityAdvertisementsScreen from "./all-tab/AllByCityAdvertisementsScreen";
import { OneAdvertisementScreen } from "../advertisement-one/OneAdvertisementScreen";
import UserProfileScreen from "../user/profile/UserProfileScreen";
import UserRatingScreen from "../user/rating/UserRatingScreen";
import UserFollowingScreen from "../user/following/UserFollowingScreen";
import UserFollowersScreen from "../user/followers/UserFollowersScreen";
import AdvertisementOffersScreen from "../offer/AdvertisementOffersScreen";
import AllByFollowingAdvertisementsScreen from "./all-following-tab/AllByFollowingAdvertisementsScreen";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

function HomeTabs() {
  return (
    <Tab.Navigator
      initialLayout={{
        width: Dimensions.get("window").width - 20,
        height: Dimensions.get("window").height - 20,
        margin: 40,
      }}
    >
      <Tab.Screen name="All" component={AllByCityAdvertisementsScreen} />
      <Tab.Screen
        name="Following"
        component={AllByFollowingAdvertisementsScreen}
      />
    </Tab.Navigator>
  );
}

export default function HomeScreenNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={"HomeTabs"}
        component={HomeTabs}
        options={{
          title: "Boonker",
          headerLeft: null,
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"OneAdvertisementScreen"}
        component={OneAdvertisementScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"AdvertisementOffersScreen"}
        component={AdvertisementOffersScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"UserProfileScreen"}
        component={UserProfileScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"UserRatingScreen"}
        component={UserRatingScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"UserFollowingScreen"}
        component={UserFollowingScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"UserFollowersScreen"}
        component={UserFollowersScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
    </Stack.Navigator>
  );
}
