import * as React from "react";

import {
  Alert,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import * as Constants from "../../Constants";

import { useDispatch, useSelector } from "react-redux";
import { Avatar, Button, Dialog, Portal, TextInput } from "react-native-paper";
import { Formik } from "formik";

import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { getCurrentUser } from "../../api/security/auth";
import { newOffer } from "../../api/offer/newOffer";
import { getOneAdvertisement } from "../advertisements-update/UpdateAdvertisementsScreen.slice";
import ErrorField from "../../components/common/ErrorField";
import LoadingIcon from "../../components/common/LoadingIcon";
import { getExpoPushToken } from "../../api/expo-notifications/getExpoPushToken";
import { sendPushNotification } from "../../components/hooks/sendPushNotification";
import call from "react-native-phone-call";

export function OneAdvertisementScreen({ route, navigation }) {
  const { id, userId } = route.params;
  const dispatch = useDispatch();
  const [finishedLoading, setFinishedLoading] = React.useState(false);

  const [visible, setVisible] = React.useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);

  React.useEffect(() => {
    async function fetchAd() {
      await dispatch(getOneAdvertisement(id));
      setFinishedLoading(true);
    }

    fetchAd();
  }, [dispatch]);

  const oneAdvertisement = useSelector(
    (state) => state.updateAdvertisement.advertisement
  );

  return !finishedLoading ? (
    <LoadingIcon />
  ) : (
    <View style={style.container}>
      <View
        style={{
          marginLeft: 14,
        }}
      >
        <View>
          <Text
            style={{
              fontSize: 28,
              marginTop: 14,
              justifyContent: "space-between",
            }}
          >
            {oneAdvertisement.title}
          </Text>
          <TouchableOpacity
            onPress={async () => {
              const userData = await getCurrentUser();
              if (
                oneAdvertisement.userId.toString() !== userData.id.toString()
              ) {
                navigation.navigate("UserProfileScreen", {
                  id: oneAdvertisement.userId,
                });
              } else {
                navigation.navigate("Profile");
              }
            }}
          >
            <View style={style.rowContainer}>
              <Avatar.Image
                size={42}
                source={require("../assets/avatar.png")}
              />
              <Text
                style={{
                  fontSize: 16,
                  marginLeft: 8,
                }}
              >
                {oneAdvertisement.userFullName}
              </Text>
            </View>
          </TouchableOpacity>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <MaterialIcons
              name="publish"
              size={28}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                alignItems: "flex-end",
                color: "gray",
              }}
            >
              Published: {oneAdvertisement.createdAt}
            </Text>
          </View>
          <TouchableOpacity
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
            onPress={() =>
              call({
                number: oneAdvertisement.userPhoneNumber,
                prompt: true,
              }).catch(console.error)
            }
          >
            <Ionicons
              name="call"
              size={18}
              style={{ marginRight: 8 }}
              color={Constants.PRIMARY_COLOR}
            />
            <Text style={{ marginLeft: 4, fontSize: 18 }}>
              {oneAdvertisement.userPhoneNumber}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Ionicons
              name="information-circle"
              size={28}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
                color: "black",
              }}
            >
              {oneAdvertisement.advertisementType} packages
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Ionicons
              name={"location"}
              size={26}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              {oneAdvertisement.cityFrom}, {oneAdvertisement.cityFromAddress}
            </Text>
            <Ionicons
              name="arrow-forward"
              size={20}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
              }}
            >
              {oneAdvertisement.advertisementType === "DRIVING" ? (
                <>{oneAdvertisement.cityTo}</>
              ) : (
                <>
                  {oneAdvertisement.cityTo}, {oneAdvertisement.cityToAddress}
                </>
              )}
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Ionicons name="cash" size={26} color={Constants.PRIMARY_COLOR} />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              {oneAdvertisement.price} kn
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <MaterialCommunityIcons
              name="package-variant-closed"
              size={26}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              {oneAdvertisement.packageSize} kg
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
            }}
          >
            <Ionicons name="car" size={24} color={Constants.PRIMARY_COLOR} />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              Delivery on: {oneAdvertisement.deliveryDate}
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              alignItems: "baseline",
            }}
          >
            <Ionicons
              name="information-circle"
              size={24}
              color={Constants.PRIMARY_COLOR}
            />
            <Text
              style={{
                fontSize: 16,
                justifyContent: "space-between",
                marginLeft: 4,
              }}
            >
              {oneAdvertisement.content}
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: "flex-end",
          margin: 14,
        }}
      >
        <Button mode="contained" onPress={showDialog}>
          Apply!
        </Button>
        <Portal>
          <Formik
            initialValues={{
              packageSize: "0",
              address: "",
              message: "",
            }}
            onSubmit={async (values, { resetForm }) => {
              await newOffer(id, values);
              const currentUser = await getCurrentUser();
              const token = await getExpoPushToken(userId);

              await sendPushNotification(
                token.expoPushToken,
                "New offer",
                currentUser.fullName +
                  " left you a new offer for your advertisement " +
                  oneAdvertisement.title +
                  "!"
              );

              Alert.alert("", "Sent successfully!", [
                { text: "OK", onPress: () => console.log("OK Pressed") },
              ]);

              hideDialog();
              resetForm();
            }}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
            }) => (
              <Dialog visible={visible} onDismiss={hideDialog}>
                <Dialog.Title>Contact user!</Dialog.Title>
                <Dialog.Content>
                  <SafeAreaView
                    style={{
                      alignItems: "center",
                    }}
                  >
                    {oneAdvertisement.advertisementType === "DRIVING" ? (
                      <>
                        <SafeAreaView style={style.textInputView}>
                          {errors.packageSize && touched.packageSize ? (
                            <ErrorField errorText={errors.packageSize} />
                          ) : null}

                          <TextInput
                            value={values.packageSize}
                            placeholder={"Your package size (kg)"}
                            onChangeText={handleChange("packageSize")}
                            onBlur={handleBlur("packageSize")}
                            keyboardType={"numeric"}
                            style={{
                              height: 40,
                              width: 280,
                              marginBottom: 12,
                              borderRadius: 20,
                              overflow: "hidden",
                              borderTopLeftRadius: 20,
                              borderTopRightRadius: 20,
                            }}
                            underlineColor={"transparent"}
                          />
                        </SafeAreaView>
                        <SafeAreaView style={style.textInputView}>
                          {errors.address && touched.address ? (
                            <ErrorField errorText={errors.address} />
                          ) : null}
                          <TextInput
                            value={values.address}
                            placeholder={"Your delivery address"}
                            onChangeText={handleChange("address")}
                            onBlur={handleBlur("address")}
                            style={{
                              height: 40,
                              width: 280,
                              marginBottom: 12,
                              borderRadius: 20,
                              overflow: "hidden",
                              borderTopLeftRadius: 20,
                              borderTopRightRadius: 20,
                            }}
                            underlineColor={"transparent"}
                          />
                        </SafeAreaView>
                      </>
                    ) : null}
                    <SafeAreaView style={style.textInputView}>
                      {errors.message && touched.message ? (
                        <ErrorField errorText={errors.message} />
                      ) : null}
                      <TextInput
                        value={values.message}
                        placeholder={"Message"}
                        multiline={true}
                        numberOfLines={4}
                        onChangeText={handleChange("message")}
                        onBlur={handleBlur("message")}
                        style={{
                          width: 280,
                          marginBottom: 12,
                          borderRadius: 20,
                          overflow: "hidden",
                          borderTopLeftRadius: 20,
                          borderTopRightRadius: 20,
                        }}
                        underlineColor={"transparent"}
                      />
                    </SafeAreaView>
                  </SafeAreaView>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button mode="contained" onPress={handleSubmit}>
                    Submit
                  </Button>
                  <Button
                    style={{ marginLeft: 4 }}
                    mode={"contained"}
                    labelStyle={{
                      color: "white",
                    }}
                    color={Constants.SECONDARY_COLOR}
                    onPress={hideDialog}
                  >
                    Cancel
                  </Button>
                </Dialog.Actions>
              </Dialog>
            )}
          </Formik>
        </Portal>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  rowContainer: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  textInputView: { alignItems: "flex-start" },
});
