import * as React from "react";

import { ActivityIndicator, View } from "react-native";

import * as Constants from "../../../Constants";

import { useDispatch, useSelector } from "react-redux";
import { getUserDrivingDeliveries } from "./DrivingPackagesScreen.slice";
import AllDeliveries from "../../../components/AllDeliveries";

export default function DrivingPackagesScreen({ navigation }) {
  const [loading, setFinishedLoading] = React.useState(false);
  const dispatch = useDispatch();
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchDrivingDeliveries() {
      await dispatch(getUserDrivingDeliveries());
      setFinishedLoading(true);
    }

    fetchDrivingDeliveries();
  }, [dispatch, refreshToggle]);

  const { drivingDeliveries } = useSelector(
    (state) => state.userDrivingDeliveries
  );

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <AllDeliveries
      navigation={navigation}
      deliveries={drivingDeliveries}
      isDriving={true}
      refresh={refresh}
      onRefresh={() => onRefreshData()}
    />
  );
}
