import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getDrivingDeliveries } from "../../../api/delivery/getDrivingDeliveries";

export const getUserDrivingDeliveries = createAsyncThunk(
  "getUserDrivingDeliveries",
  getDrivingDeliveries
);

export const initialState = {
  drivingDeliveries: [],
  loading: false,
  error: null,
};

export const userDrivingDeliveriesSlice = createSlice({
  name: "userDrivingDeliveriesSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserDrivingDeliveries.fulfilled, (state, action) => {
      state.drivingDeliveries = action.payload;
    });
  },
});

const userDrivingDeliveries = userDrivingDeliveriesSlice.reducer;

export default userDrivingDeliveries;
