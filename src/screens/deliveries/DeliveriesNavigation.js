import * as React from "react";

import { Dimensions } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import DrivingPackagesScreen from "./driving-packages/DrivingPackagesScreen";
import IncomingPackagesScreen from "./incoming-packages/IncomingPackagesScreen";
import DriverTrackingScreen from "../tracking/driver-delivery-tracking/DriverTrackingScreen";
import IncomingDeliveryTracking from "../tracking/incoming-delivery-tracking/IncomingDeliveryTracking";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

function DeliveriesTabs() {
  return (
    <Tab.Navigator
      initialLayout={{
        width: Dimensions.get("window").width - 20,
        height: Dimensions.get("window").height - 20,
        margin: 40,
      }}
    >
      <Tab.Screen name="Driving" component={DrivingPackagesScreen} />
      <Tab.Screen
        name="Sending and Incoming"
        component={IncomingPackagesScreen}
      />
    </Tab.Navigator>
  );
}

export default function DeliveriesNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={"DeliveriesTabs"}
        component={DeliveriesTabs}
        options={{
          title: "Boonker",
          headerLeft: null,
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"DriverTrackingScreen"}
        component={DriverTrackingScreen}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
      <Stack.Screen
        name={"IncomingDeliveryTracking"}
        component={IncomingDeliveryTracking}
        options={{
          title: "Boonker",
          headerTitleStyle: { alignSelf: "center" },
        }}
      />
    </Stack.Navigator>
  );
}
