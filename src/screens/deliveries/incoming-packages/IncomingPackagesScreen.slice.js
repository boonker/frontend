import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getIncomingDeliveries } from "../../../api/delivery/getIncomingDeliveries";

export const getUserIncomingDeliveries = createAsyncThunk(
  "getUserIncomingDeliveries",
  getIncomingDeliveries
);

export const initialState = {
  incomingDeliveries: [],
  loading: false,
  error: null,
};

export const userIncomingDeliveriesSlice = createSlice({
  name: "userIncomingDeliveriesSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserIncomingDeliveries.fulfilled, (state, action) => {
      console.log("REDUCER", state, action);
      state.incomingDeliveries = action.payload;
    });
  },
});

const userIncomingDeliveries = userIncomingDeliveriesSlice.reducer;

export default userIncomingDeliveries;
