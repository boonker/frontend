import * as React from "react";

import { ActivityIndicator, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import { getUserIncomingDeliveries } from "./IncomingPackagesScreen.slice";
import * as Constants from "../../../Constants";
import AllDeliveries from "../../../components/AllDeliveries";

export default function IncomingPackagesScreen({ navigation }) {
  const [loading, setFinishedLoading] = React.useState(false);
  const dispatch = useDispatch();
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchIncomingDeliveries() {
      await dispatch(getUserIncomingDeliveries());
      setFinishedLoading(true);
    }

    fetchIncomingDeliveries();
  }, [dispatch, refreshToggle]);

  const { incomingDeliveries } = useSelector(
    (state) => state.userIncomingDeliveries
  );

  console.log("incomingDeliveries", incomingDeliveries);

  return loading === false ? (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator
        animating={true}
        size={"large"}
        color={Constants.PRIMARY_COLOR}
      />
    </View>
  ) : (
    <AllDeliveries
      navigation={navigation}
      deliveries={incomingDeliveries}
      isDriving={false}
      onRefresh={() => onRefreshData()}
      refresh={refresh}
    />
  );
}
