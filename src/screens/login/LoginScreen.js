import * as React from "react";

import { Formik } from "formik";
import { Image, ImageBackground, SafeAreaView } from "react-native";

import { Button, Text, TextInput } from "react-native-paper";

import * as Constants from "../../Constants";
import AuthService from "../../api/security/auth";

import * as Yup from "yup";
import ErrorField from "../../components/common/ErrorField";
import registerForPushNotificationsAsync from "../../components/hooks/registerForPushNotificationsAsync";
import { setExpoPushToken } from "../../api/expo-notifications/setExpoPushToken";

const LoginSchema = Yup.object().shape({
  username: Yup.string().required("Required!"),
  password: Yup.string().required("Required!"),
});

export default function LoginScreen(props) {
  const [error, setError] = React.useState(false);
  const [secureEntry, setSecureEntry] = React.useState(true);

  const onEyePress = () => {
    setSecureEntry(!secureEntry);
  };

  const onRegister = () => {
    props.navigation.navigate("RegistrationScreen");
  };

  return (
    <ImageBackground
      source={require("../assets/delicate.jpg")}
      resizeMode="cover"
      style={{ flex: 1, justifyContent: "center" }}
    >
      <SafeAreaView
        style={{
          margin: 50,
          alignItems: "center",
        }}
      >
        <Image
          source={require("../assets/boonker.png")}
          style={{
            resizeMode: "stretch",
          }}
        />
      </SafeAreaView>
      <Formik
        initialValues={{
          username: "",
          password: "",
        }}
        onSubmit={async (values, { resetForm }) => {
          const response = await AuthService.login(
            values.username,
            values.password
          );

          if (response.status === 200 && response.error === false) {
            registerForPushNotificationsAsync().then(
              async (token) => await setExpoPushToken(token)
            );

            props.navigation.navigate("AppNavigation");
            resetForm();
            setError(false);
          } else {
            setError(true);
          }
        }}
        validationSchema={LoginSchema}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <SafeAreaView
            style={{
              alignItems: "center",
            }}
          >
            <SafeAreaView
              style={{
                alignItems: "flex-start",
              }}
            >
              {errors.username && touched.username ? (
                <ErrorField errorText={errors.username} />
              ) : null}
              <TextInput
                value={values.username}
                placeholder={"Username"}
                onChangeText={handleChange("username")}
                onBlur={handleBlur("username")}
                mode={"flat"}
                underlineColor={"transparent"}
                style={{
                  height: 40,
                  width: 300,
                  marginBottom: 12,
                  borderRadius: 20,
                  overflow: "hidden",
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}
                name={"username"}
              />
            </SafeAreaView>
            <SafeAreaView>
              {errors.password && touched.password ? (
                <ErrorField errorText={errors.password} />
              ) : null}
              <TextInput
                value={values.password}
                placeholder={"Password"}
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                secureTextEntry={secureEntry}
                underlineColor={"transparent"}
                right={
                  <TextInput.Icon
                    name="eye"
                    color={Constants.PRIMARY_COLOR}
                    onPress={onEyePress}
                  />
                }
                style={{
                  height: 40,
                  width: 300,
                  marginBottom: 20,
                  borderRadius: 20,
                  overflow: "hidden",
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}
                name={"password"}
              />
            </SafeAreaView>
            {error ? (
              <Text style={{ color: "red", marginBottom: 8 }}>
                Wrong username or password, try again!
              </Text>
            ) : null}
            <Button
              onPress={handleSubmit}
              mode="contained"
              contentStyle={{
                width: 200,
              }}
            >
              Sign in
            </Button>
            <Text style={{ marginTop: 20 }}>Don't have an account? </Text>
            <Text
              style={{ color: Constants.PRIMARY_COLOR, margin: 2 }}
              onPress={onRegister}
            >
              Sign up now!
            </Text>
          </SafeAreaView>
        )}
      </Formik>
    </ImageBackground>
  );
}
