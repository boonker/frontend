import * as React from "react";

import { Formik } from "formik";
import {
  Alert,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from "react-native";
import { Button, Text, TextInput } from "react-native-paper";

import ErrorField from "../../components/common/ErrorField";
import * as Constants from "../../Constants";
import AuthService from "../../api/security/auth";

import * as Yup from "yup";

const RegistrationSchema = Yup.object().shape({
  fullName: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  city: Yup.string()
    .min(3, "Too short! Should be 3 chars min")
    .max(80, "Too song! Should be 80 chars max")
    .required("Required!"),
  age: Yup.number()
    .min(18, "Sorry! You should be at least 18 years old!")
    .required("Required!"),
  phoneNumber: Yup.string().required("Required!"),
  email: Yup.string().email("Must be a valid email").required("Required!"),
  username: Yup.string()
    .required("Required!")
    .min(5, "Too short! Should be 5 chars min")
    .max(80, "Too song! Should be 80 chars max"),
  password: Yup.string()
    .required("Required!")
    .min(5, "Too short! Should be 5 chars min")
    .max(80, "Too song! Should be 80 chars max"),
});

export default function RegistrationScreen(props) {
  const [error, setError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");
  const [secureEntry, setSecureEntry] = React.useState(true);

  const onEyePress = () => {
    setSecureEntry(!secureEntry);
  };

  const onLogin = () => {
    props.navigation.navigate("LoginScreen");
  };

  return (
    <ImageBackground
      source={require("../assets/delicate.jpg")}
      resizeMode="cover"
      style={{ flex: 1, justifyContent: "center" }}
    >
      <ScrollView>
        <SafeAreaView
          style={{
            margin: 50,
            alignItems: "center",
          }}
        >
          <Image
            source={require("../assets/boonker.png")}
            style={{
              resizeMode: "stretch",
            }}
          />
        </SafeAreaView>
        <Formik
          initialValues={{
            fullName: "",
            username: "",
            password: "",
            email: "",
            city: "",
            age: "",
            phoneNumber: "",
          }}
          onSubmit={async (values, resetForm) => {
            const response = await AuthService.register(values);

            if (response.message !== "") {
              setError(true);
              setErrorMessage(response.message);
            } else {
              Alert.alert(
                "Registration",
                "Successfully registered, you can sign in now!"
              );
              setError(false);
              setErrorMessage("");
              resetForm();
            }
          }}
          validationSchema={RegistrationSchema}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <SafeAreaView
              style={{
                alignItems: "center",
              }}
            >
              <SafeAreaView style={style.textInputView}>
                {errors.fullName && touched.fullName ? (
                  <ErrorField errorText={errors.fullName} />
                ) : null}
                <TextInput
                  value={values.fullName}
                  placeholder={"Full name"}
                  onChangeText={handleChange("fullName")}
                  onBlur={handleBlur("fullName")}
                  style={style.textInputStyle}
                  name={"name"}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.fullName && touched.fullName ? (
                  <ErrorField errorText={errors.fullName} />
                ) : null}
                <TextInput
                  value={values.city}
                  placeholder={"City"}
                  onChangeText={handleChange("city")}
                  onBlur={handleBlur("city")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.age && touched.age ? (
                  <ErrorField errorText={errors.age} />
                ) : null}
                <TextInput
                  value={values.age}
                  placeholder={"Age"}
                  onChangeText={handleChange("age")}
                  onBlur={handleBlur("age")}
                  keyboardType={"numeric"}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.phoneNumber && touched.phoneNumber ? (
                  <ErrorField errorText={errors.phoneNumber} />
                ) : null}
                <TextInput
                  value={values.phoneNumber}
                  placeholder={"Phone number"}
                  onChangeText={handleChange("phoneNumber")}
                  onBlur={handleBlur("phoneNumber")}
                  keyboardType={"numeric"}
                  name={"phoneNumber"}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.email && touched.email ? (
                  <ErrorField errorText={errors.email} />
                ) : null}
                <TextInput
                  value={values.email}
                  placeholder={"Email"}
                  keyboardType={"email-address"}
                  style={style.textInputStyle}
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.username && touched.username ? (
                  <ErrorField errorText={errors.username} />
                ) : null}
                <TextInput
                  value={values.username}
                  placeholder={"Username"}
                  onChangeText={handleChange("username")}
                  onBlur={handleBlur("username")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                />
              </SafeAreaView>
              <SafeAreaView style={style.textInputView}>
                {errors.password && touched.password ? (
                  <ErrorField errorText={errors.password} />
                ) : null}
                <TextInput
                  value={values.password}
                  placeholder={"Password"}
                  secureTextEntry={secureEntry}
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  style={style.textInputStyle}
                  underlineColor={"transparent"}
                  right={
                    <TextInput.Icon
                      name="eye"
                      color={Constants.PRIMARY_COLOR}
                      onPress={onEyePress}
                    />
                  }
                />
              </SafeAreaView>
              {error ? (
                <Text style={{ color: "red", marginBottom: 8 }}>
                  {errorMessage}
                </Text>
              ) : null}
              <Button
                onPress={handleSubmit}
                mode="contained"
                contentStyle={{
                  width: 200,
                }}
              >
                Register
              </Button>
              <Text style={{ marginTop: 20 }}>Already have an account?</Text>
              <Text
                style={{ color: Constants.PRIMARY_COLOR, margin: 2 }}
                onPress={onLogin}
              >
                Sign in!
              </Text>
            </SafeAreaView>
          )}
        </Formik>
      </ScrollView>
    </ImageBackground>
  );
}

const style = StyleSheet.create({
  textInputStyle: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderRadius: 20,
    overflow: "hidden",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textInputView: { alignItems: "flex-start" },
});
