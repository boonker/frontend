import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { getAllAdvertisementOffers } from "../../api/offer/allAdvertisementOffers";

export const getAdvertisementOffers = createAsyncThunk(
  "getAdvertisementOffers",
  async (id) => await getAllAdvertisementOffers(id)
);

export const initialState = {
  offers: [],
  reservedBoonkerSize: 0,
  advertisedBoonkerSize: 0,
  advertisementType: "",
  loading: false,
  error: null,
};

export const advertisementOffersSlice = createSlice({
  name: "advertisementOffersSlice",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAdvertisementOffers.fulfilled, (state, action) => {
      console.log("REDUCER", action.payload);
      state.offers = action.payload.offers;
      state.reservedBoonkerSize = action.payload.reservedBoonkerSize;
      state.advertisedBoonkerSize = action.payload.advertisedBoonkerSize;
      state.advertisementType = action.payload.advertisementType;
    });
  },
});

const advertisementOffers = advertisementOffersSlice.reducer;

export default advertisementOffers;
