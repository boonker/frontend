import * as React from "react";

import {
  Alert,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";

import { useDispatch, useSelector } from "react-redux";
import { Avatar, Button, Card } from "react-native-paper";

import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

import { getAdvertisementOffers } from "./AdvertisementOffersScreen.slice";
import { acceptOffer } from "../../api/offer/acceptOffer";
import { declineOffer } from "../../api/offer/declineOffer";
import LoadingIcon from "../../components/common/LoadingIcon";

const LeftContent = () => (
  <Avatar.Image size={48} source={require("../assets/avatar.png")} />
);

const Item = ({ item, onAccept, navigation, onDecline }) => {
  const onAcceptAlert = () => {
    Alert.alert("Accept offer", "Are you sure?", [
      { text: "Yes", onPress: () => onAccept(item.id) },
      { text: "Cancel" },
    ]);
  };

  const onDeclineAlert = () => {
    Alert.alert("Decline offer", "Are you sure?", [
      { text: "Yes", onPress: () => onDecline(item.id) },
      { text: "Cancel" },
    ]);
  };

  return (
    <SafeAreaView
      style={{
        margin: 4,
      }}
    >
      <Card
        style={{
          borderWidth: 1,
        }}
      >
        <Card.Title
          title={item.createdByFullName}
          subtitle={<Text>Sent: {item.publishedOn}</Text>}
          left={LeftContent}
          right={() => {
            if (item.offerType === "ACCEPTED") {
              return (
                <Ionicons
                  style={{
                    marginRight: 8,
                  }}
                  name={"checkmark-circle"}
                  size={24}
                  color={"green"}
                />
              );
            }
            if (item.offerType === "DECLINED") {
              return (
                <Feather
                  style={{ marginRight: 8 }}
                  name="x-circle"
                  size={24}
                  color="red"
                />
              );
            }
          }}
        />
        <Card.Content>
          <View
            style={{
              justifyContent: "space-between",
              flexDirection: "row",
              marginBottom: 2,
              marginTop: 4,
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "baseline",
              }}
            >
              <Text
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
                style={styles.info}
              >
                {item.message}
              </Text>
            </View>
            <Text>{item.packageSize} kg</Text>
          </View>
        </Card.Content>
        <Card.Actions style={{ marginLeft: 8 }}>
          {item.offerType === "PENDING" ? (
            <>
              <Button
                mode={"contained"}
                style={{ marginRight: 4 }}
                onPress={onAcceptAlert}
              >
                Accept
              </Button>
              <Button mode={"contained"} onPress={onDeclineAlert} color={"red"}>
                Decline
              </Button>
            </>
          ) : null}
        </Card.Actions>
      </Card>
    </SafeAreaView>
  );
};

export default function AdvertisementOffersScreen({ route, navigation }) {
  const { id } = route.params;

  const [loading, setFinishedLoading] = React.useState(false);
  const [selectedId, setSelectedId] = React.useState(null);
  const dispatch = useDispatch();
  const [refresh, setRefresh] = React.useState(false);
  const [refreshToggle, setRefreshToggle] = React.useState(false);

  React.useEffect(() => {
    if (refresh) {
      setRefreshToggle(!refreshToggle);
    }
    setRefresh(false);
  }, [refresh]);

  const onRefreshData = () => {
    setRefresh(true);
  };

  const onAcceptOffer = async (itemId) => {
    await acceptOffer(itemId);
    setSelectedId(Math.random());
  };

  const onDeclineOffer = async (itemId) => {
    await declineOffer(itemId);
    setSelectedId(Math.random());
  };

  React.useEffect(() => {
    setFinishedLoading(false);

    async function fetchOffers() {
      await dispatch(getAdvertisementOffers(id));
      setFinishedLoading(true);
    }

    fetchOffers();
  }, [dispatch, selectedId, refreshToggle]);

  const {
    offers,
    reservedBoonkerSize,
    advertisedBoonkerSize,
    advertisementType,
  } = useSelector((state) => state.advertisementOffers);

  const renderItem = ({ item }) => (
    <Item
      item={item}
      navigation={navigation}
      onAccept={(itemId) => onAcceptOffer(itemId)}
      onDecline={(itemId) => onDeclineOffer(itemId)}
    />
  );

  return loading === false ? (
    <LoadingIcon />
  ) : (
    <SafeAreaView style={styles.container}>
      {advertisementType === "DRIVING" ? (
        <View style={{ marginLeft: 12 }}>
          {reservedBoonkerSize === advertisedBoonkerSize ||
          reservedBoonkerSize > advertisedBoonkerSize ? (
            <>
              <Text style={{ fontWeight: "bold" }}>You are full! </Text>
              <Text>
                You reserved {reservedBoonkerSize} kg of advertised{" "}
                {advertisedBoonkerSize} kg in your boonker!
              </Text>
            </>
          ) : (
            <Text>
              You reserved {reservedBoonkerSize} kg of advertised{" "}
              {advertisedBoonkerSize} kg in your boonker!
            </Text>
          )}
        </View>
      ) : null}
      <FlatList
        data={offers}
        extraData={selectedId}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        onRefresh={onRefreshData}
        refreshing={refresh}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 4,
    marginHorizontal: 12,
  },
  info: {
    fontSize: 16,
    marginLeft: 8,
  },
});
