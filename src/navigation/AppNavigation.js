import * as React from "react";

import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import * as Constants from "../Constants";

import ProfileScreen from "../screens/profile/ProfileScreen";
import HomeScreenNavigation from "../screens/home/HomeScreenNavigation";
import NewAdvertisementScreen from "../screens/advertisement-new/NewAdvertisementScreen";
import DeliveriesNavigation from "../screens/deliveries/DeliveriesNavigation";
import MyAdvertisementsNavigationScreen from "../screens/advertisements-my/MyAdvertisementsNavigationScreen";

export default function AppNavigation() {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          switch (route.name) {
            case "Home":
              iconName = focused ? "home" : "home-outline";
              break;
            case "Deliveries":
              iconName = focused ? "car" : "car-outline";
              break;
            case "My":
              iconName = focused ? "reader" : "reader-outline";
              break;
            case "New":
              iconName = focused ? "add-circle" : "add-circle-outline";
              break;
            case "Profile":
              iconName = focused ? "person-circle" : "person-circle-outline";
              break;
            default:
              iconName = "";
              break;
          }

          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: Constants.PRIMARY_COLOR,
        inactiveTintColor: "gray",
        labelStyle: {
          margin: 2,
        },
      }}
    >
      <Tab.Screen name="Home" component={HomeScreenNavigation} />
      <Tab.Screen name="Deliveries" component={DeliveriesNavigation} />
      <Tab.Screen name="New" component={NewAdvertisementScreen} />
      <Tab.Screen name="My" component={MyAdvertisementsNavigationScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}
